//
//  AVChatingVC.swift
//  Basic-Video-Chat
//
//  Created by Sahil Kumar on 16/07/19.
//  Copyright © 2019 tokbox. All rights reserved.
//

import UIKit

class AVChatingVC: UIViewController {
    
    private let config = Config.default

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func webRTCBtnTap(_ sender :Any) {
    
        let vc = buildMainViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func buildMainViewController() -> UIViewController {
        let signalClient = SignalingClient(serverUrl: self.config.signalingServerUrl)
        let webRTCClient = WebRTCClient(iceServers: self.config.webRTCIceServers)
        if let vc = UIStoryboard(name: "Storyboard", bundle: Bundle.main).instantiateViewController(withIdentifier: "MainViewController") as? MainViewController
        {
            vc.configure(signalClient: signalClient, webRTCClient: webRTCClient)
           
            return vc
        } else {
            return UIViewController()
        }
    }

}
