//
//  APIManager.swift
//  Basic-Video-Chat
//
//  Created by Sahil Kumar on 07/05/19.
//  Copyright © 2019 tokbox. All rights reserved.
//

import Foundation

class APIManager {
    class func getToken(id:String,completion: @escaping (_ token: String,_ error: Bool)->()) {
        var request = URLRequest(url: URL(string: TwilioApiConstants.base+TwilioApiConstants.getToken + "?identity=\(id)")!)
        
        request.httpMethod = ApiMethodType.post
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil {
                
                guard let content = String.init(data: data!, encoding: String.Encoding.utf8) else {
                    return
                }
                completion(content ,false)
            } else {
                AlertControllers.alert(message: "Issue occured while registering.")
                completion("",true)
            }
        }.resume()
    }
}
