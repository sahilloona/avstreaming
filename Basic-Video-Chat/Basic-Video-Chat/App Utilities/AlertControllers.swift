//
//  AlertControllers.swift
//  Basic-Video-Chat
//
//  Created by Sahil Kumar on 06/05/19.
//  Copyright © 2019 tokbox. All rights reserved.
//

import UIKit

class  AlertControllers {
    /// set top vc
    ///
    /// - Returns: top view controller
    class func topMostController() -> UIViewController? {
        
        var presentedVC = UIApplication.shared.keyWindow?.rootViewController
        while let pVC = presentedVC?.presentedViewController {
            presentedVC = pVC
        }
        
        if presentedVC == nil {
            //print("AlertController Error: You don't have any views set. You may be calling in viewdidload. Try viewdidappear.")
        } else if let presentedNVC = presentedVC as? UINavigationController,
            let topVc = presentedNVC.topViewController {
            presentedVC = topVc
        }
        return presentedVC
    }
    
    class func alert(message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: UIAlertController.Style.alert)
        let action = UIAlertAction(title: "Ok", style: .cancel)
        alert.addAction(action)
        self.topMostController()?.present(alert, animated: true, completion: nil)
    }
   
}
