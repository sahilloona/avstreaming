//
//  AppConstants.swift
//  Basic-Video-Chat
//
//  Created by Sahil Kumar on 25/04/19.
//  Copyright © 2019 tokbox. All rights reserved.
//

import Foundation

struct OpenTokApiConstants {
    static var apiUrlToCreateSession = "https://api.opentok.com/session/create"
    static var apiId = "219edbae388f5bb73aded49c74171fa63861f1d3"
    static var secretId = "46311322"
    static var urlToCreateToken = "https://tokbox.com/developer/tools/playground/?v=2.15&session_id="
}

struct TwilioApiConstants {
    static let base = "https://40561c8e.ngrok.io/api"
    static let getToken = "/twilioaudio/accessToken.php"
}

struct ApiMethodType {
    static var get = "GET"
    static var post = "POST"
}

struct SegueIdentifier {
    static var tokenGeneratorSegue = "tokengenerator"
    static var showCallTypeSegue = "option"
    static var stresmingStartSegue = "streaming"
}
