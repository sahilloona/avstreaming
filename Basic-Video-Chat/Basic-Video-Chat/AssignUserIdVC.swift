//
//  SinchVCViewController.swift
//  Basic-Video-Chat
//
//  Created by Chicmic on 18/04/19.
//  Copyright © 2019 tokbox. All rights reserved.
//

import UIKit
//import Sinch
//import SinchVerification

class AssignUserIdVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var activityView: UIActivityIndicatorView!
    @IBOutlet weak var userIdTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
   
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    @IBAction func submitUserId(_ senderId: Any){
        activityView.isHidden = false
        activityView.startAnimating()
        if let userId = userIdTextField.text, !userId.isEmpty{
            SinchManager.shared.registerSinchClient(userId: userId){[weak self] in
                self?.activityView.stopAnimating()
                 self?.performSegue(withIdentifier: SegueId.registrationToMakeACall, sender: nil)
            }
           
        } else {
            let alert = UIAlertController(title: AlertTitle.sinch, message: AlertMessage.userId, preferredStyle: .alert)
            
            self.present(alert, animated: true)
            alert.addAction(UIAlertAction.init(title: AlertTitle.ok, style: .cancel, handler: nil))
            
        }
    }
  
    
    
}
