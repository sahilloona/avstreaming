//
//  IncomingCallVC.swift
//  Basic-Video-Chat
//
//  Created by Chicmic on 19/04/19.
//  Copyright © 2019 tokbox. All rights reserved.
//

import UIKit

class IncomingCallVC: UIViewController {

    @IBOutlet weak var userIdLabel: UILabel!
    var callMode : CallMode = .audio
    var callRecieved :Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userIdLabel.text = SinchManager.shared.callerName
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        checkCallRecieved()
    }
    
    func removeSubView() {
        self.willMove(toParent: nil)
        self.view.removeFromSuperview()
        self.removeFromParent()
    }
    
    @IBAction func pickUp(_ senderId: Any){
        self.callMode = SinchManager.shared.getCallMode()
        SinchManager.shared.pickUpCall()
        
        switch self.callMode {
        case .audio:
            performSegue(withIdentifier: SegueId.IncomingToOngoingAudioCall, sender: self)
        case .video:
            performSegue(withIdentifier: SegueId.IncomingToOngoingVideoCall, sender: self)
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueId.IncomingToOngoingVideoCall {
            if let dest = segue.destination as? OnGoingVideoCallVC {
                SinchManager.shared.showVideoControllerDelegate = dest
                dest.completion = {
                  //  self.dismiss(animated: true, completion: nil)
                }
            }
        } else if segue.identifier == SegueId.IncomingToOngoingAudioCall {
            if let dest = segue.destination as? OnGoingCallVC {
                SinchManager.shared.showVideoControllerDelegate = dest
            }
        }
        self.callRecieved = true
    }

    
    
    @IBAction func hangUp(_ senderId: Any){
        SinchManager.shared.hangUpCall()
        removeSubView()
    }
    
    private func checkCallRecieved(){
        if self.callRecieved {
            removeSubView()
        }
    }

}
