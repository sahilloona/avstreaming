//
//  MakeACall.swift
//  Basic-Video-Chat
//
//  Created by Chicmic on 18/04/19.
//  Copyright © 2019 tokbox. All rights reserved.
//

import UIKit


class MakeACallVC: UIViewController {
    
    @IBOutlet weak var userIdTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func makeCall(_ senderId: Any){
        if let userId = userIdTextField.text, !userId.isEmpty {
            SinchManager.shared.assignCallerId(callerID: userId)
            SinchManager.shared.callMode = .audio
            if let callerId = SinchManager.shared.callerId {
                SinchManager.shared.calling(withUserId: callerId)
            }
        }
        else {
            let alert = UIAlertController(title: AlertTitle.sinch, message: AlertMessage.callerId, preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: AlertTitle.ok, style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func onVideoCallTapped(_ sender: Any) {
        if let userId = userIdTextField.text, !userId.isEmpty {
            SinchManager.shared.assignCallerId(callerID: userId)
            SinchManager.shared.callMode = .video
            if let callerId = SinchManager.shared.callerId {
                SinchManager.shared.calling(withUserId: callerId)
            }
        }
        else {
            let alert = UIAlertController(title: AlertTitle.sinch, message: AlertMessage.callerId, preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: AlertTitle.ok, style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueId.makeACallToVideoCall {
            if let dest = segue.destination as? OnGoingVideoCallVC {
                SinchManager.shared.showVideoControllerDelegate = dest
            }
        } else if segue.identifier == SegueId.makeACallToVideoCall {
            if let dest = segue.destination as? OnGoingCallVC {
                SinchManager.shared.showVideoControllerDelegate = dest
            }
        }
    }
    
}
