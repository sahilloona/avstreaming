//
//  OnGoingCallVC.swift
//  Basic-Video-Chat
//
//  Created by Chicmic on 18/04/19.
//  Copyright © 2019 tokbox. All rights reserved.
//

import UIKit

protocol OngoingCallProtocolDelegate {
    func assignUserId()
}

class OnGoingCallVC: UIViewController {

    var userId = String()
    var delegate: OngoingCallProtocolDelegate?

    @IBOutlet weak var userIdLabel: UILabel!
    @IBOutlet weak var callingLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let callerId = SinchManager.shared.callerId,
            !(callerId.trimmingCharacters(in: .whitespaces).isEmpty) {
            userIdLabel.text = SinchManager.shared.callerId
        } else {
            userIdLabel.text = SinchManager.shared.callerName
        }
    //    SinchManager.shared.calling(withUserId: SinchManager.shared.callerId)
        SinchManager.shared.desableSpeakerMode()
        print("call")

    }

    @IBAction func hangUp(_ senderId: Any){
        SinchManager.shared.hangUpCall()
        self.dismiss(animated: true) {
        }
    }
}
 
extension OnGoingCallVC : showAudioVideoController{
    func callConnected() {
        callingLabel.text = callStatus.callConnected
    }
    
    func callDeclined() {
        self.dismiss(animated: true, completion: nil)
        self.navigationController?.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    func callDisconnected() {
        let alert = UIAlertController(title: AlertTitle.sinch, message: AlertMessage.callDisconnect, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: AlertTitle.ok, style: .cancel){(alert) in
            self.dismiss(animated: true, completion: {
            })
        })
        self.present(alert, animated: true, completion: nil)
    }
    
    
}
