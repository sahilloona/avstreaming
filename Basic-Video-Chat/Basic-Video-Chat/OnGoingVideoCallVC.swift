//
//  OnGoingCallVC.swift
//  Basic-Video-Chat
//
//  Created by Chicmic on 18/04/19.
//  Copyright © 2019 tokbox. All rights reserved.
//

import UIKit


class OnGoingVideoCallVC: UIViewController {
    
    @IBOutlet weak var MaxView: UIView!
    @IBOutlet weak var minView: UIView!
    var userId = String()
    var delegate: OngoingCallProtocolDelegate?
    
    
    @IBOutlet weak var userIdLabel: UILabel!
    @IBOutlet weak var callingLabel: UILabel!
    var completion : ()->Void = {}
    override func viewDidLoad() {
        super.viewDidLoad()
        if let callerId = SinchManager.shared.callerId,
            !(callerId.trimmingCharacters(in: .whitespaces).isEmpty) {
            userIdLabel.text = SinchManager.shared.callerId
        } else {
            userIdLabel.text = SinchManager.shared.callerName
        }
        SinchManager.shared.enableSpeakerMode()
        //    SinchManager.shared.calling(withUserId: SinchManager.shared.callerId)
    }
    
    @IBAction func hangUp(_ senderId: Any){
        SinchManager.shared.hangUpCall()
    }
}

extension OnGoingVideoCallVC : showAudioVideoController {

    func callConnected() {
        userIdLabel.isHidden = true
        callingLabel.isHidden = true
        let view1 = SinchManager.shared.videoController.remoteView()
        view1?.frame = CGRect(x: 0, y: 0, width: MaxView.frame.width, height: MaxView.frame.height)
        MaxView.addSubview(view1!)
        let clentView = SinchManager.shared.videoController.localView()
        clentView?.frame = CGRect(x: 0, y: 0, width: minView.frame.width, height: minView.frame.height)
        minView.addSubview(clentView!)
        self.view.layoutIfNeeded()
    }
    
    func callDisconnected() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func callDeclined() {
        self.dismiss(animated: true, completion: nil)
    }
    
}

