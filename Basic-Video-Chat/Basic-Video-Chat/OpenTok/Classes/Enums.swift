//
//  Enums.swift
//  Basic-Video-Chat
//
//  Created by Sahil Kumar on 25/04/19.
//  Copyright © 2019 tokbox. All rights reserved.
//

import Foundation

/// tell in which mode you are going to use it
///
/// - create: you will create your own room
/// - join: you will join the existing room
enum Mode {
    case create
    case join
}

enum ChatType {
    case audio
    case video
}
