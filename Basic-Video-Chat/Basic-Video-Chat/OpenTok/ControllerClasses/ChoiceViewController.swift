//
//  ChoiceViewController.swift
//  Basic-Video-Chat
//
//  Created by Puneeta Mishra on 23/04/19.
//  Copyright © 2019 tokbox. All rights reserved.
//

import UIKit

class ChoiceViewController: UIViewController {
    private var chatType : ChatType = .video
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

// MARK:- OPTIONS ACTIVITY (AUDIO/VIDEO)

extension ChoiceViewController {
    /// to set audio only chat
    ///
    /// - Parameter sender:
    @IBAction func onAudioTapped(_ sender :Any?){
        self.chatType = .audio
        performSegue(withIdentifier: SegueIdentifier.stresmingStartSegue, sender: self)
    }
    
    /// to set video streaming
    ///
    /// - Parameter sender:
    @IBAction func onVideoTapped(_ sender :Any?){
        self.chatType = .video
        performSegue(withIdentifier: SegueIdentifier.stresmingStartSegue, sender: self)
    }
}

// MISCELLANEOUS ACTIVITIES
extension ChoiceViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let dest = segue.destination as? LiveSessionVC else {
            return
        }
        dest.setChatType(type: chatType)
    }
}
