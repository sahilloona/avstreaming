//
//  LiveSessionVC.swift
//
//  Created by Sahil Kumar on 19/04/19.
//  Copyright © 2019 tokbox. All rights reserved.
//

import UIKit
import OpenTok

// *** Fill the following variables using your own Project info  ***
// ***            https://tokbox.com/account/#/                  ***
// Replace with your OpenTok API key
let kApiKey = "46311322"
// Replace with your generated session ID
var kSessionId = ""
// Replace with your generated token
var kToken = ""

class LiveSessionVC: UIViewController {
    // Properties
    private var chatType: ChatType = .video
    @IBOutlet weak var subscriberLabel: UILabel!
    @IBOutlet weak var disconnectBtn: UIButton!
    @IBOutlet weak var videoDisconnectBtn: UIButton!
    @IBOutlet weak var streamingView: UIView!
    lazy var session: OTSession = {
        return OTSession(apiKey: kApiKey, sessionId: kSessionId, delegate: self)!
    }()
    lazy var publisher: OTPublisher = {
        let settings = OTPublisherSettings()
        settings.name = UIDevice.current.name
        return OTPublisher(delegate: self, settings: settings)!
    }()
    var subscriber: OTSubscriber?
    
    // Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setTopLabel()
        doConnect()
    }
    
   
}
//MARK:- UI SETUP AND ALERT
extension LiveSessionVC {
    func setTopLabel(){
        switch chatType {
        case .audio:
            self.navigationItem.title = "Audio Chat"
        case .video:
            self.navigationItem.title = "Video Chat"
        }
    }
    
    func setChatType(type: ChatType){
        self.chatType = type
    }
    
    private func showAlert(message: String){
        let controller = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        controller.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(controller, animated: true, completion: nil)
    }
}

//MARK: - CONNECT,PUBLISH AND SUBSCRIBE ACTIONS
extension LiveSessionVC {
    /**
     * Asynchronously begins the session connect process. Some time later, we will
     * expect a delegate method to call us back with the results of this action.
     */
    fileprivate func doConnect() {
        var error: OTError?
        defer {
            processError(error)
        }
        session.connect(withToken: kToken, error: &error)
    }
    
    /**
     * Sets up an instance of OTPublisher to use with this session. OTPubilsher
     * binds to the device camera and microphone, and will provide A/V streams
     * to the OpenTok session.
     */
    fileprivate func doPublish() {
        var error: OTError?
        defer {
            processError(error)
        }
        switch chatType {
        case .audio:
            streamingView.isHidden = true
            publisher.publishVideo = false
            publisher.publishAudio = true
        case .video:
            streamingView.isHidden = false
            publisher.publishVideo = true
            publisher.publishAudio = true
        }
        
        session.publish(publisher, error: &error)
        
        // to show view for video only for video streaming
        switch chatType {
        case .audio:
            break
        case .video:
            if let pubView = publisher.view {
                pubView.frame = CGRect(x: 0, y: 0, width: self.streamingView.frame.width, height: self.streamingView.frame.height)
                streamingView.addSubview(pubView)
            }
        }
    }
    
    /**
     * Instantiates a subscriber for the given stream and asynchronously begins the
     * process to begin receiving A/V content for this stream. Unlike doPublish,
     * this method does not add the subscriber to the view hierarchy. Instead, we
     * add the subscriber only after it has connected and begins receiving data.
     */
    fileprivate func doSubscribe(_ stream: OTStream) {
        var error: OTError?
        defer {
            processError(error)
        }
        subscriber = OTSubscriber(stream: stream, delegate: self)
        
        switch chatType {
        case .audio:
            if stream.hasVideo {
                showAlert(message: "Subscriber is connected with video ")
                
            }
            else {
                session.subscribe(subscriber!, error: &error)
            }
            subscriberLabel.text = stream.name
        case .video:
            if !stream.hasVideo {
                showAlert(message: "Subscriber is not connected with video ")
                
            } else{
                session.subscribe(subscriber!, error: &error)
            }
        }
    }
    
    fileprivate func processError(_ error: OTError?) {
        if let err = error {
            DispatchQueue.main.async {
                self.showAlert(message: err.localizedDescription)
            }
        }
    }
}

//MARK:- CLEANUP ACTIVITIES
extension LiveSessionVC {
    fileprivate func cleanupSubscriber() {
        switch chatType {
        case .audio:
            disconnectBtn.isHidden = true
            subscriberLabel.text = "Disconnected"
        case .video:
            subscriber?.view?.removeFromSuperview()
        }
        subscriber = nil
    }
    
    fileprivate func cleanupPublisher() {
        publisher.view?.removeFromSuperview()
    }
}

// MARK: - OTSession delegate callbacks
extension LiveSessionVC: OTSessionDelegate {
    func sessionDidConnect(_ session: OTSession) {
        print("Session connected")
        doPublish()
    }
    
    func sessionDidDisconnect(_ session: OTSession) {
        print("Session disconnected")
    }
    
    func session(_ session: OTSession, streamCreated stream: OTStream) {
        print("Session streamCreated: \(stream.streamId)")
        if subscriber == nil {
            doSubscribe(stream)
        }
    }
    
    func session(_ session: OTSession, streamDestroyed stream: OTStream) {
        print("Session streamDestroyed: \(stream.streamId)")
        if let subStream = subscriber?.stream, subStream.streamId == stream.streamId {
            cleanupSubscriber()
        }
    }
    
    func session(_ session: OTSession, didFailWithError error: OTError) {
        print("session Failed to connect: \(error.localizedDescription)")
    }
}

// MARK: - OTPublisher delegate callbacks
extension LiveSessionVC: OTPublisherDelegate {
    func publisher(_ publisher: OTPublisherKit, streamCreated stream: OTStream) {
        print("Publishing")
    }
    
    func publisher(_ publisher: OTPublisherKit, streamDestroyed stream: OTStream) {
        cleanupPublisher()
        if let subStream = subscriber?.stream, subStream.streamId == stream.streamId {
            cleanupSubscriber()
        }
    }
    
    func publisher(_ publisher: OTPublisherKit, didFailWithError error: OTError) {
        print("Publisher failed: \(error.localizedDescription)")
    }
}

// MARK: - OTSubscriber delegate callbacks
extension LiveSessionVC: OTSubscriberDelegate {
    func subscriberDidConnect(toStream subscriberKit: OTSubscriberKit) {
        switch chatType {
        case .audio:
            disconnectBtn.isHidden = false
            subscriberLabel.isHidden = false
            videoDisconnectBtn.isHidden = true
        case .video:
            if let pubView = publisher.view {
                pubView.frame = CGRect(x: 0, y: 0, width: self.streamingView.frame.width, height: self.streamingView.frame.height/2)
            }
            if let subsView = subscriber?.view {
                subsView.frame = CGRect(x: 0, y: self.streamingView.frame.height/2, width: self.streamingView.frame.width, height: self.streamingView.frame.height/2)
                streamingView.addSubview(subsView)
            }
        }
        
        view.layoutIfNeeded()
    }
    
    func subscriber(_ subscriber: OTSubscriberKit, didFailWithError error: OTError) {
        print("Subscriber failed: \(error.localizedDescription)")
    }
}

// MARK:- UI ACTIONS
extension LiveSessionVC {
    
    @IBAction func onSessionCloseClicked(_ sender: Any) {
        session.disconnect(nil)
        self.navigationController?.popToRootViewController(animated: true)
    }
    @IBAction func onChoiceClicked(_ sender: Any) {
        session.disconnect(nil)
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onInviteClicked(_ sender: Any?){
        let activityViewController = UIActivityViewController(activityItems: ["Join me using SessionID = "+kSessionId], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.streamingView // so that iPads won't crash
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
        self.present(activityViewController, animated: true, completion: nil)
    }
}

