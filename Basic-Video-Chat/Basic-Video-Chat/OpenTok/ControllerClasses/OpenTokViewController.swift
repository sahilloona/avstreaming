//
//  OpenTokViewController.swift
//  Basic-Video-Chat
//
//  Created by SAHIL KUMAR on 18/04/19.
//  Copyright © 2019 tokbox. All rights reserved.
//

import UIKit

class OpenTokViewController: UIViewController {
    
    private var sessionId = ""
    private var activity = UIActivityIndicatorView()
    private var mode : Mode = .create
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addActivity()
    }
    
    /// Create Session Id API Call
    ///
    /// - Parameter completion: One Session Created and SessionId recieved it Calls Completion Block
    func createSession(completion: @escaping ()->Void){
        
        var request = URLRequest(url: URL(string: OpenTokApiConstants.apiUrlToCreateSession)!)
        request.httpMethod = ApiMethodType.post
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("\(OpenTokApiConstants.secretId):\(OpenTokApiConstants.apiId)", forHTTPHeaderField: "X-TB-PARTNER-AUTH")
        
        let task = URLSession.shared.dataTask(with: request){[weak self] (data,response,error) in
            guard let _ = response , let dat = data else {
                self?.stopActivity()
                self?.showAlert(message: error?.localizedDescription ?? "")
                return
            }
            guard let  d = try? JSONSerialization.jsonObject(with: dat, options: []) as? [[String:Any?]]  else {
                return
            }
            self?.sessionId = d![0]["session_id"] as! String
            //    UserDefaults.setValue(kSessionId, forKey: "session")
            completion()
        }
        task.resume()
    }
}

// MARK:-  CREATE AND JOIN ROOM ACTIVITY
extension OpenTokViewController {
    
    @IBAction func createRoomAction(_ sender: Any) {
        mode = .create
        startActivity() // start indicator
        createSession{[weak self] in
            DispatchQueue.main.async {
                self?.stopActivity()
                self?.performSegue(withIdentifier: SegueIdentifier.tokenGeneratorSegue, sender: nil)
            }
        }
    }
    
    @IBAction func onJoinRoomClicked(_ sender: Any) {
        mode = .join
        let alert = UIAlertController(title: "OpenTok", message: "enter Session ID", preferredStyle: .alert)
        alert.addTextField{(textField ) in
            textField.placeholder = "Session Id"
        }
        alert.addAction(UIAlertAction(title: "Enter into Room", style: .default){[weak self] (finished) in
            guard let text =  alert.textFields?[0].text else {
                return
            }
            print(text)
            if !text.isEmpty {
                kSessionId = text
            } else {
                return
            }
            
            self?.performSegue(withIdentifier: SegueIdentifier.tokenGeneratorSegue, sender: nil)
            
        })
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
// MARK:- Activity Indicator

extension OpenTokViewController {
    
    /// Start loading Activity
    private func startActivity(){
        self.view.addSubview(activity)
        activity.startAnimating()
        self.view.isUserInteractionEnabled = false
    }
    
    /// Stop Loading Activity
    private func stopActivity(){
        DispatchQueue.main.async {
            self.activity.stopAnimating()
            self.activity.removeFromSuperview()
            self.view.isUserInteractionEnabled = true
        }
        
    }
}

// MISCELLANEOUS ACTIVITIES

extension OpenTokViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let dest = segue.destination as? TokenViewController else {
            return
        }
        switch mode {
        case .create:
            kSessionId = self.sessionId
            dest.setUrl()
        case .join:
            dest.setUrl()
        }
    }
    
    private func showAlert(message: String){
        let controller = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        controller.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(controller, animated: true, completion: nil)
    }
    
    /// adding Loader for activity going on backend
    func addActivity() {
        activity.frame = CGRect(x: self.view.frame.width/2 - 80/2 , y: self.view.frame.height/2 - 80/2, width: 80, height: 80)
        activity.style = .whiteLarge
        activity.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
    }
}
