//
//  TokenViewController.swift
//  Basic-Video-Chat
//
//  Created by Puneeta Mishra on 18/04/19.
//  Copyright © 2019 tokbox. All rights reserved.
//

import UIKit
import WebKit

class TokenViewController: UIViewController {

    var url :String = ""
    let userName = "Sahil.kumar@chicmic.co.in"
    let password = "ChicMic2019"
    
    @IBOutlet weak var proceedBtnOutlet: UIButton!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var webView: UIWebView?
    override func viewDidLoad() {
        super.viewDidLoad()
        loadWebview()
        uiSetup()
    }
    
    private func uiSetup() {
        proceedBtnOutlet.setTitle("Waiting...", for: .disabled)
        proceedBtnOutlet.setTitle("Enter to Proceed", for: .normal)
        proceedBtnOutlet.setTitleColor(UIColor.black, for: .disabled)
        proceedBtnOutlet.setTitleColor(UIColor.white, for: .normal)
    }
    
   private func loadWebview(){
    webView?.scalesPageToFit = true
    webView?.loadRequest(URLRequest(url: URL(string: url)!))
    }
    
    /// called When segue is perfromed by previous Controller
    func setUrl() {
        url = OpenTokApiConstants.urlToCreateToken + kSessionId
    }
}

//MARK:-  WEBVIEW DELEGATES
extension TokenViewController:UIWebViewDelegate {
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        addActivityIndicator()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
        webView.stringByEvaluatingJavaScript(from: "document.getElementById('user_email').value = '\(userName)'; document.getElementById('user_password').value= '\(password)'")
        webView.stringByEvaluatingJavaScript(from: "document.getElementsByClassName('btn btn-blue ctaarrow-white')[0].click()")
        
        webView.stringByEvaluatingJavaScript(from: "document.getElementsByName('session_id')[0].value = '\(kSessionId)'")
        
        removeActivityIndicator()
        
        guard let x = webView.stringByEvaluatingJavaScript(from:"document.getElementById('token').value" ) else {
            return
        }
        if !x.isEmpty {
            kToken = x
            performSegue(withIdentifier: SegueIdentifier.showCallTypeSegue, sender: self)
        }
    }
}

// MARK: - LOADER ACTIVITY
extension TokenViewController {
    private func addActivityIndicator(){
        activity.isHidden = false
        activity.startAnimating()
        self.view.isUserInteractionEnabled = false
        proceedBtnOutlet.isEnabled = false
        proceedBtnOutlet.backgroundColor = UIColor.white
    }
    private func removeActivityIndicator(){
        activity.stopAnimating()
        self.view.isUserInteractionEnabled = true
        proceedBtnOutlet.isEnabled = true
        proceedBtnOutlet.backgroundColor = UIColor.lightGray
        
    }
}

// MARK:- UI ACTIONS
extension TokenViewController {
    @IBAction func proceedButton(_ sender: Any) {
        webView?.stringByEvaluatingJavaScript(from: "document.getElementById('join_session_submit').click()")
    }
}
