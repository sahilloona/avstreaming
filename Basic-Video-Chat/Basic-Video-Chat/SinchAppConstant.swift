//
//  SinchAppConstant.swift
//  Basic-Video-Chat
//
//  Created by Chicmic on 30/04/19.
//  Copyright © 2019 tokbox. All rights reserved.
//

import UIKit

struct StoryBoard{
    static var incomingCall: UIStoryboard {
        get {
            return UIStoryboard(name: "Main", bundle: nil)
        }
    }
    
    
}

struct  ViewControllerId {
    static let incomingCallVC = "IncomingCallVC"
}

struct callStatus {
    static let callConnected = "Call Connected"
}

struct SegueId{
    static let registrationToMakeACall = "registrationToMakeACall"
    static let makeACallToVideoCall = "makeACallToVideoCall"
    static let makeACallToAudioCall = "makeACallToAudioCall"
    static let IncomingToOngoingVideoCall = "IncomingToOngoingVideoCall"
    static let IncomingToOngoingAudioCall = "IncomingToOngoingAudioCall"
}

struct AlertTitle{
    static let notifiactionalertActionTitle = "answere"
    static let sinch = "Sinch"
    static let ok = "Ok"

}

struct AlertMessage {
    static let notifiactionalertBody = "Incoming call"
    static let userId = "Please Enter User Id"
    static let callerId = "Please Enter CallerId"
    static let callDisconnect = "Call is Declined"
}

struct SinchKeys {
    static let appKey = "72aef4e7-2672-4f2c-bbe2-56026e95ce51"
    static let envHost = "clientapi.sinch.com"
    static let appSecret = "OrFxJ7vzWU6snfLi9ow0pw=="
}
