//
//  SinchManager.swift
//  Basic-Video-Chat
//
//  Created by Chicmic on 19/04/19.
//  Copyright © 2019 tokbox. All rights reserved.
//

import UIKit
import Sinch

enum CallMode {
    case audio
    case video
}

@objc protocol showAudioVideoController {
    func callConnected()
    func callDeclined()
    func callDisconnected()
    }



class SinchManager: NSObject, SINClientDelegate, SINCallDelegate, SINCallClientDelegate {
    
    var topVC: UIViewController!
    var callerId: String?
    var sinchClient: SINClient!
    var callersCall: SINCall?
    var call: SINCall?
    var callClient: SINCallClient!
    static let shared = SinchManager()
    var callMode: CallMode = .audio
    var completion: ()->Void = {}
    var callerName: String?
    
    weak var showVideoControllerDelegate : showAudioVideoController?
    
    var videoController : SINVideoController {
        return self.sinchClient.videoController()
    }
    var audioController : SINAudioController {
        return self.sinchClient.audioController()
    }
    
    private override init() {
        super.init()
    }
    
    private func clientCapabilities() {
        sinchClient.setSupportCalling(true)
        
        sinchClient.enableManagedPushNotifications()
    }
    
    private func assignDelegate() {
        sinchClient.delegate = self
        sinchClient.call()?.delegate = self
    }
    
    private func enstablishConnection() {
        sinchClient.start()
        sinchClient.startListeningOnActiveConnection()
    }
    
    func hangUpCall() {
        if let cal = callersCall {
            cal.hangup()
        }
        if let cal = self.call {
            cal.hangup()
        }
       // sinchClient.stop()
    //    sinchClient.stopListeningOnActiveConnection()
    }
    
    func assignCallerId(callerID: String) {
        callerId = callerID
    }
    
    func registerSinchClient(userId: String,completion: @escaping ()->Void) {
        self.completion = completion
        sinchClient = Sinch.client(withApplicationKey: SinchKeys.appKey, applicationSecret: SinchKeys.appSecret, environmentHost: SinchKeys.envHost, userId: "\(userId)")
        self.clientCapabilities()
        self.enstablishConnection()
        self.assignDelegate()
    }
    
    func pickUpCall() {
        
        callersCall?.answer()
        callersCall?.delegate = self
    }
    
    func getCallMode()->CallMode {
        if (callersCall?.details.isVideoOffered)! {
            return .video
        } else {
            return .audio
        }
    }
    
    // MARK: SINClientDelegate
    func calling(withUserId userId: String) {
        
        
        switch callMode {
        case .audio:
            call = sinchClient.call()?.callUser(withId: userId)
        case .video:
            call = sinchClient.call()?.callUserVideo(withId: userId)
        }
        call?.delegate = self
    }
    
    func clientDidStart(_ client: SINClient!) {
        self.completion()
    }
    
    func clientDidFail(_ client: SINClient!, error: Error!) {
    }
    func callDidAddVideoTrack(_ call: SINCall!) {
    }
    func callDidEstablish(_ call: SINCall!) {
        // status Outlet
        callersCall = call
        showVideoControllerDelegate?.callConnected()
        // callback to the
    }
    
    func callDidProgress(_ call: SINCall!) {
        // outgoing call is in process
    }
    //logic for presenting a local notification if the app is in the background when receiving the call
    func client(_ client: SINCallClient!, localNotificationForIncomingCall call: SINCall!) -> SINLocalNotification! {
        let notification = SINLocalNotification()
        notification.alertAction = AlertTitle.notifiactionalertActionTitle
        notification.alertBody = AlertMessage.notifiactionalertBody
        return notification
    }
    
    func client(_ client: SINClient!, requiresRegistrationCredentials registrationCallback: SINClientRegistration!) {
        
    }
    
    func client(_ client: SINClient!, logMessage message: String!, area: String!, severity: SINLogSeverity, timestamp: Date!) {
        
    }
    func callDidEnd(_ call: SINCall!) {
        showVideoControllerDelegate?.callDeclined()
    }
    func clientDidStop(_ client: SINClient!) {
        showVideoControllerDelegate?.callDisconnected()
    }
    func client(_ client: SINCallClient!, didReceiveIncomingCall call: SINCall!) {
        callersCall = call
       callerName = call.remoteUserId
    
        let storyBoard = StoryBoard.incomingCall
        let vc = storyBoard.instantiateViewController(withIdentifier: ViewControllerId.incomingCallVC)
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            topController.addChild(vc)
            topController.view.addSubview(vc.view)
            vc.didMove(toParent: topController)
        }
//        call.answer()
    }
    
    func call(_ call: SINCall!, shouldSendPushNotifications pushPairs: [Any]!) {
        
    }
    

}

extension SinchManager {
    func enableSpeakerMode() {
        self.audioController.enableSpeaker()
    }
    func desableSpeakerMode() {
        self.audioController.disableSpeaker()
    }
}

