//
//  TwilioAudioRGVC.swift
//  Basic-Video-Chat
//
//  Created by Sahil Kumar on 07/05/19.
//  Copyright © 2019 tokbox. All rights reserved.
//

import UIKit

class TwilioAudioRGVC: UIViewController {
    @IBOutlet weak var loader: UIActivityIndicatorView!
    
    @IBOutlet weak var field: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func submitAction(_ sender: Any) {
        
        if let id = field.text,!id.isEmpty {
            startLoader()
           identity = id
            APIManager.getToken(id: id) { (string,err) in
                self.stopLoading()
                if let er = err as? Bool, er {
                    return
                }
                if let token = string as? String {

                    accessToken = token
                    DispatchQueue.main.async {
                        self.performSegue(withIdentifier: "callPage", sender: sender)
                    }
                }
            }
            
            
            }
        }
}

extension TwilioAudioRGVC {
    private func startLoader(){
        self.loader.isHidden = false
        self.loader.startAnimating()
        self.view.isUserInteractionEnabled = false
    }
    
    private func stopLoading(){
        DispatchQueue.main.async {
             self.loader.stopAnimating()
            self.view.isUserInteractionEnabled = true
        }
        
    }
}
