//
//  TwilioVideoManager.swift
//  Basic-Video-Chat
//
//  Created by Sahil Kumar on 06/05/19.
//  Copyright © 2019 tokbox. All rights reserved.
//

import UIKit
import TwilioVoice
import PushKit
import UserNotifications
import AVFoundation
import  CallKit


var accessToken = ""
var identity = "" // caller

let twimlParamTo = "to"


enum CallingMode {
    case incoming
    case outgoing
}

protocol CallingDelegatePrototcol {
    func callConnected()
    func callDisconnected()
}

protocol IncomingCallRecievedDelegate {
    func CallRecieved()
}

class TwilioVoiceManager:NSObject, PKPushRegistryDelegate, TVONotificationDelegate, TVOCallDelegate, AVAudioPlayerDelegate, UITextFieldDelegate {
    
  
   static let shared = TwilioVoiceManager()
    
    var CallType : CallingMode = .outgoing
    
    var ongoingCallDelegate : CallingDelegatePrototcol? 
    var callRecievedDelegate: IncomingCallRecievedDelegate?
   
    @IBOutlet weak var callControlView: UIView!

    var deviceTokenString:String?

    var voipRegistry:PKPushRegistry?
    var incomingPushCompletionCallback: (()->Swift.Void?)? = nil

    var incomingAlertController: UIAlertController?

    var callInvite:TVOCallInvite?
    var call:TVOCall?

    var ringtonePlayer:AVAudioPlayer?
    var ringtonePlaybackCallback: (() -> ())?
    
    func setupPushN() {
        voipRegistry = PKPushRegistry.init(queue: DispatchQueue.main)
        voipRegistry?.delegate = self
        voipRegistry?.desiredPushTypes = Set([PKPushType.voIP])
    }


  

    ///  to fetch token from server
    ///
    /// - Returns: return token after calling api
    func fetchAccessToken() -> String? {
        let endpointWithIdentity = String(format: "%@?identity=%@", TwilioApiConstants.getToken, identity)
        guard let accessTokenURL = URL(string: TwilioApiConstants.base + endpointWithIdentity) else {
            return nil
        }
        return try? String.init(contentsOf: accessTokenURL, encoding: .utf8)
    }
    
  

     func placeCall(user: String) {
            self.makeCall(user)
            self.CallType = .outgoing
        }
    
    func hangUpCall() {
        if (self.call != nil) {
            self.call?.disconnect()
        }
    }
    
    func makeCall(_ to: String) {
        if let token = fetchAccessToken() {
            accessToken = token
        }
        let connectOptions = TVOConnectOptions(accessToken: accessToken) { (builder) in
            builder.params = [twimlParamTo : to]
        }

        self.checkRecordPermission { (permissionGranted) in
            if (!permissionGranted) {
               self.permissionAlert(connectOptions: connectOptions)
            } else {
                self.call = TwilioVoice.connect(with: connectOptions, delegate: self)
            }
        }
    }
    
    func checkRecordPermission(completion: @escaping (_ permissionGranted: Bool) -> Void) {
        let permissionStatus: AVAudioSession.RecordPermission = AVAudioSession.sharedInstance().recordPermission
        
        switch permissionStatus {
        case AVAudioSession.RecordPermission.granted:
            // Record permission already granted.
            completion(true)
            break
        case AVAudioSession.RecordPermission.denied:
            // Record permission denied.
            completion(false)
            break
        case AVAudioSession.RecordPermission.undetermined:
            // Requesting record permission.
            // Optional: pop up app dialog to let the users know if they want to request.
            AVAudioSession.sharedInstance().requestRecordPermission({ (granted) in
                completion(granted)
            })
            break
        default:
            completion(false)
            break
        }
    }

   
    // MARK: PKPushRegistryDelegate
    func pushRegistry(_ registry: PKPushRegistry, didUpdate credentials: PKPushCredentials, for type: PKPushType) {
        NSLog("pushRegistry:didUpdatePushCredentials:forType:");
        
        if (type != .voIP) {
            return
        }
        
        if let token = fetchAccessToken()  {
            accessToken = token
        }
        
        let deviceToken = (credentials.token as NSData).description
        
        TwilioVoice.register(withAccessToken: accessToken, deviceToken: deviceToken) { (error) in
            if let error = error {
                NSLog("An error occurred while registering: \(error.localizedDescription)")
            }
            else {
                NSLog("Successfully registered for VoIP push notifications.")
            }
        }

        self.deviceTokenString = deviceToken
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didInvalidatePushTokenFor type: PKPushType) {
        NSLog("pushRegistry:didInvalidatePushTokenForType:")
        
        if (type != .voIP) {
            return
        }
        
        guard let deviceToken = deviceTokenString else {
            return
        }
        
        TwilioVoice.unregister(withAccessToken: accessToken, deviceToken: deviceToken) { (error) in
            if let error = error {
                NSLog("An error occurred while unregistering: \(error.localizedDescription)")
            }
            else {
                NSLog("Successfully unregistered from VoIP push notifications.")
            }
        }
        
        self.deviceTokenString = nil
    }
    
    /**
     * Try using the `pushRegistry:didReceiveIncomingPushWithPayload:forType:withCompletionHandler:` method if
     * your application is targeting iOS 11. According to the docs, this delegate method is deprecated by Apple.
     */
    func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType) {
        NSLog("pushRegistry:didReceiveIncomingPushWithPayload:forType:")
        
        if (type == PKPushType.voIP) {
            TwilioVoice.handleNotification(payload.dictionaryPayload, delegate: self)
        }
    }
    
    /**
     * This delegate method is available in iOS 11 and above. Call the completion handler once the
     * notification payload is passed to the `TwilioVoice.handleNotification()` method.
     */
    func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType, completion: @escaping () -> Void) {
        NSLog("pushRegistry:didReceiveIncomingPushWithPayload:forType:completion:")
        // Save for later when the notification is properly handled.
        self.incomingPushCompletionCallback = completion

        if (type == PKPushType.voIP) {
            TwilioVoice.handleNotification(payload.dictionaryPayload, delegate: self)
        }
    }
    
    func incomingPushHandled() {
        if let completion = self.incomingPushCompletionCallback {
            completion()
            self.incomingPushCompletionCallback = nil
        }
    }
    
    // MARK: TVONotificaitonDelegate
    @available(iOS 10.0, *)
    func callInviteReceived(_ callInvite: TVOCallInvite) {
        NSLog("callInviteReceived:")

        if (self.callInvite != nil) {
            NSLog("A CallInvite is already in progress. Ignoring the incoming CallInvite from \(callInvite.from)")
            self.incomingPushHandled()
            return
        } else if (self.call != nil && self.call?.state == .connected) {
            NSLog("Already an active call. Ignoring incoming CallInvite from \(callInvite.from)");
            self.incomingPushHandled()
            return;
        }
        
        self.callInvite = callInvite;
        
        let from = callInvite.from
        let alertMessage = "From: \(String(describing: from!.dropFirst("client:".count)))"
    
        
        let incomingAlertController = UIAlertController(title: "Incoming",
                                                        message: alertMessage,
                                                        preferredStyle: .alert)
        
        let rejectAction = UIAlertAction(title: "Reject", style: .default) { [weak self] (action) in
            if let strongSelf = self {
                callInvite.reject()
                strongSelf.callInvite = nil

                strongSelf.incomingAlertController = nil
                
            }
        }
        incomingAlertController.addAction(rejectAction)
        
        let ignoreAction = UIAlertAction(title: "Ignore", style: .default) { [weak self] (action) in
            if let strongSelf = self {
                /* To ignore the CallInvite, you don't have to do anything but just literally ignore it */

                strongSelf.callInvite = nil
                strongSelf.incomingAlertController = nil
            }
        }
        incomingAlertController.addAction(ignoreAction)

        let acceptAction = UIAlertAction(title: "Accept", style: .default) { [weak self] (action) in
            if let strongSelf = self {
                self?.CallType = .incoming
                let acceptOptions: TVOAcceptOptions = TVOAcceptOptions(callInvite: callInvite) { (builder) in
                    builder.uuid = strongSelf.callInvite?.uuid
                }
                strongSelf.call = callInvite.accept(with: acceptOptions, delegate: strongSelf)
                strongSelf.callInvite = nil
                strongSelf.incomingAlertController = nil
                strongSelf.callRecievedDelegate?.CallRecieved()

            }
        }
        incomingAlertController.addAction(acceptAction)
        
        AlertControllers.topMostController()?.present(incomingAlertController, animated: true, completion: nil)
        self.incomingAlertController = incomingAlertController
        self.incomingCallInBackground()
        self.incomingPushHandled()
    }
    
    func incomingCallInBackground() {
        // If the application is not in the foreground, post a local notification
        if (UIApplication.shared.applicationState != UIApplication.State.active) {
            let content = UNMutableNotificationContent()
            content.title = "Incoming Call"
            content.body = "Call Invite from \(String(describing: self.callInvite?.from!.dropFirst("client:".count)))"
            content.sound = UNNotificationSound.default
            
            let request = UNNotificationRequest(identifier: "VoiceLocaNotification",
                                                content: content, trigger: nil)
            
            let center = UNUserNotificationCenter.current()
            center.add(request) { (error) in
                if (error != nil) {
                    print("Failed to add notification reqeust: \(error!.localizedDescription)")
                }
            }
        }
    }
    
    
    func cancelledCallInviteReceived(_ cancelledCallInvite: TVOCancelledCallInvite) {
        NSLog("cancelledCallInviteCanceled:")
        
        if (self.callInvite == nil ||
            self.callInvite!.callSid != cancelledCallInvite.callSid) {
            NSLog("No matching pending CallInvite. Ignoring the Cancelled CallInvite")
            return
        }


        if (incomingAlertController != nil) {
            incomingAlertController?.dismiss(animated: true) { [weak self] in
                if let strongSelf = self {
                    strongSelf.incomingAlertController = nil
                }
            }
        }
        
        self.callInvite = nil
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        } else {
            // nothing
        }
        
        self.incomingPushHandled()
    }

    // MARK: TVOCallDelegate
    func callDidConnect(_ call: TVOCall) {
        NSLog("callDidConnect:")
        self.call = call
       
        ongoingCallDelegate?.callConnected()
        
        toggleAudioRoute(toSpeaker: true)
    }
    func call(_ call: TVOCall, didFailToConnectWithError error: Error) {
        NSLog("Call failed to connect: \(error.localizedDescription)")
        
        self.callDisconnected()
    }
    
    func call(_ call: TVOCall, didDisconnectWithError error: Error?) {
        if let error = error {
            NSLog("Call failed: \(error.localizedDescription)")
        } else {
            NSLog("Call disconnected")
        }
        
        self.callDisconnected()
    }
    
    func callDisconnected() {
        self.call = nil
        ongoingCallDelegate?.callDisconnected()
    }
    
    
    // MARK: AVAudioSession
    func toggleAudioRoute(toSpeaker: Bool) {
        // The mode set by the Voice SDK is "VoiceChat" so the default audio route is the built-in receiver. Use port override to switch the route.
        let audioDevice: TVODefaultAudioDevice = TwilioVoice.audioDevice as! TVODefaultAudioDevice
        audioDevice.block = {
            kTVODefaultAVAudioSessionConfigurationBlock()
            do {
                if (toSpeaker) {
                    try AVAudioSession.sharedInstance().overrideOutputAudioPort(.speaker)
                } else {
                    try AVAudioSession.sharedInstance().overrideOutputAudioPort(.none)
                }
            } catch {
                NSLog(error.localizedDescription)
            }
        }
        audioDevice.block()
    }


 
    func notificationError(_ error: Error) {
        
    }
}

//MARK:- Alerts
extension TwilioVoiceManager {
    
    /// permission alert
    func permissionAlert(connectOptions: TVOConnectOptions) {
        let alertController: UIAlertController = UIAlertController(title: "Voice Quick Start",
                                                                   message: "Microphone permission not granted",
                                                                   preferredStyle: .alert)
        
        let continueWithMic: UIAlertAction = UIAlertAction(title: "Continue without microphone",
                                                           style: .default,
                                                           handler: { (action) in
                                                            self.call =  TwilioVoice.connect(with: connectOptions, delegate: self)
        })
        alertController.addAction(continueWithMic)
        
        let goToSettings: UIAlertAction = UIAlertAction(title: "Settings",
                                                        style: .default,
                                                        handler: { (action) in
                                                            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!,
                                                                                      options: [UIApplication.OpenExternalURLOptionsKey.universalLinksOnly: false],
                                                                                      completionHandler: nil)
        })
        alertController.addAction(goToSettings)
        
        let cancel: UIAlertAction = UIAlertAction(title: "Cancel",
                                                  style: .cancel,
                                                  handler: { (action) in
                                                    
        })
        alertController.addAction(cancel)
        
        AlertControllers.topMostController()?.present(alertController, animated: true, completion: nil)
    }
    
}
