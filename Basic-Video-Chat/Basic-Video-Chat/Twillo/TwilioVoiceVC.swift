//
//  TwilioVoiceVC.swift
//  Basic-Video-Chat
//
//  Created by Sahil Kumar on 01/05/19.
//  Copyright © 2019 tokbox. All rights reserved.
//

import UIKit
import TwilioVoice

var ACCOUNT_SID = ""
var API_KEY = ""
var API_KEY_SECRET = ""

class TwilioVoiceVC: UIViewController {
  
    let baseURLString = ""
    // If your token server is written in PHP, accessTokenEndpoint needs .php extension at the end. For example : /accessToken.php
    let accessTokenEndpoint = "/accessToken"
    let identity = "sahil"
    let twimlParamTo = "to"

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
