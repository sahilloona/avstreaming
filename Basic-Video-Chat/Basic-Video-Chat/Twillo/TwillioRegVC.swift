//
//  TwillioRegVC.swift
//  Basic-Video-Chat
//
//  Created by Sahil Kumar on 06/05/19.
//  Copyright © 2019 tokbox. All rights reserved.
//

import UIKit
import TwilioVoice

class TwillioRegVC: UIViewController {

     @IBOutlet weak var outgoingValue: UITextField!
     var callerId = ""
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        TwilioVoiceManager.shared.setupPushN()
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        TwilioVoiceManager.shared.callRecievedDelegate = self
        // Do any additional setup after loading the view.
    }
    
    @IBAction func makeaCall(_ sender:Any?){
        if let user = outgoingValue.text {
            self.callerId = user
            TwilioVoiceManager.shared.placeCall(user: user)
        }
    }
}


extension TwillioRegVC :UITextFieldDelegate {
    // MARK: UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        outgoingValue.resignFirstResponder()
        return true
    }
}

extension TwillioRegVC {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "makeCall":
            if let dest = segue.destination as? onGoingVoiceVC {
                dest.setupCaller(id: callerId)
                TwilioVoiceManager.shared.ongoingCallDelegate = dest
            }
        default:
            break
        }
    }
}

extension TwillioRegVC:IncomingCallRecievedDelegate {
    func CallRecieved() {
       
            self.performSegue(withIdentifier: "makeCall", sender: nil)
        
    }
    
    
}
