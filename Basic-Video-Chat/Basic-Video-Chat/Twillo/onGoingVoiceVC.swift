//
//  onGoingVoiceVC.swift
//  Basic-Video-Chat
//
//  Created by Sahil Kumar on 06/05/19.
//  Copyright © 2019 tokbox. All rights reserved.
//

import UIKit

class onGoingVoiceVC: UIViewController {

    @IBOutlet weak var callerId: UILabel!
    @IBOutlet weak var callingLabel: UILabel!
    var callerName: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCallerName()
    }
    
    func setupCallerName() {
        switch TwilioVoiceManager.shared.CallType {
        case .incoming:
            if let caller = TwilioVoiceManager.shared.call?.from {
                callerId.text = String(caller.dropFirst("client:".count))
            }
            
        case .outgoing:
            callerId.text = callerName
        }
    }
    
    func setupCaller(id :String) {
        self.callerName = id
    }
  @IBAction  func speakerSwitchToggled(_ sender: UISwitch) {
       TwilioVoiceManager.shared.toggleAudioRoute(toSpeaker: sender.isOn)
    }
    
    @IBAction func hangUp(_ sender: Any) {
        TwilioVoiceManager.shared.hangUpCall()
    }
}

extension onGoingVoiceVC:CallingDelegatePrototcol {
    func callConnected() {
        callingLabel.text = "Call Connected"
    }
    
    func callDisconnected() {
        callingLabel.text = "Mobile Calling..."
        self.dismiss(animated: true, completion: nil)
    }
    
    
}
