//
//  ViewController.swift
//  WebRTC
//
//  Created by Sahil Kumar on 20/05/2018.
//  Copyright © 2018 Chicmic. All rights reserved.
//

import UIKit
import AVFoundation
import WebRTC


class MainViewController: UIViewController {

    
    private var signalClient: SignalingClient?
    private var webRTCClient: WebRTCClient?
   
   weak var delegate : CallControlDelegate?
    
    weak var delegateForChat : ChatControlDelegate?
    
    @IBOutlet private weak var signalingStatusLabel: UILabel?
    @IBOutlet private weak var localSdpStatusLabel: UILabel?
    @IBOutlet private weak var localCandidatesLabel: UILabel?
    @IBOutlet private weak var remoteSdpStatusLabel: UILabel?
    @IBOutlet private weak var remoteCandidatesLabel: UILabel?
  
    @IBOutlet private weak var webRTCStatusLabel: UILabel?
    
    private var signalingConnected: Bool = false {
        didSet {
            DispatchQueue.main.async {
                if self.signalingConnected {
                    self.signalingStatusLabel?.text = "Connected"
                    self.signalingStatusLabel?.textColor = UIColor.green
                }
                else {
                    self.signalingStatusLabel?.text = "Not connected"
                    self.signalingStatusLabel?.textColor = UIColor.red
                }
            }
        }
    }
    
    private var hasLocalSdp: Bool = false {
        didSet {
            DispatchQueue.main.async {
                self.localSdpStatusLabel?.text = self.hasLocalSdp ? "✅" : "❌"
            }
        }
    }
    
    private var localCandidateCount: Int = 0 {
        didSet {
            DispatchQueue.main.async {
                self.localCandidatesLabel?.text = "\(self.localCandidateCount)"
            }
        }
    }
    
    private var hasRemoteSdp: Bool = false {
        didSet {
            DispatchQueue.main.async {
                self.remoteSdpStatusLabel?.text = self.hasRemoteSdp ? "✅" : "❌"
            }
        }
    }
    
    private var remoteCandidateCount: Int = 0 {
        didSet {
            DispatchQueue.main.async {
                self.remoteCandidatesLabel?.text = "\(self.remoteCandidateCount)"
            }
        }
    }
    
    func configure (signalClient: SignalingClient, webRTCClient: WebRTCClient) {
        self.signalClient = signalClient
        self.webRTCClient = webRTCClient
    }
    
    
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "WebRTC Demo"
        self.signalingConnected = false
        self.hasLocalSdp = false
        self.hasRemoteSdp = false
        self.localCandidateCount = 0
        self.remoteCandidateCount = 0
      
        self.webRTCStatusLabel?.text = "New"
        
        self.signalClient?.connect()
        self.webRTCClient?.delegate = self
        self.signalClient?.delegate = self
    }
    
    @IBAction private func offerDidTap(_ sender: UIButton) {
        self.webRTCClient?.offer { (sdp) in
            self.hasLocalSdp = true
            self.signalClient?.senCallType(callT: CallType.audio)
            self.signalClient?.send(sdp: sdp)
        
        }
        self.performSegue(withIdentifier: "voicecall", sender: nil)
        
    }
    
    @IBAction private func answerDidTap(_ sender: UIButton) {
        self.webRTCClient?.answer { (localSdp) in
            self.hasLocalSdp = true
            self.signalClient?.send(sdp: localSdp)
        }
    }
   
    
    @IBAction private func videoDidTap(_ sender: UIButton) {
        self.webRTCClient?.offer { (sdp) in
            self.hasLocalSdp = true
            self.signalClient?.senCallType(callT: CallType.video)
            self.signalClient?.send(sdp: sdp)
        }
        let vc = WebRTCVideoViewController(webRTCClient: self.webRTCClient!)
        self.delegate = vc
        self.present(vc, animated: true, completion: nil)
    }
    
   
    
   
    
    
}

extension MainViewController: SignalClientDelegate {
    func signalClientDidConnect(_ signalClient: SignalingClient) {
        self.signalingConnected = true
    }
    
    func signalClientDidDisconnect(_ signalClient: SignalingClient) {
        self.signalingConnected = false
    }
    
    func signalClient(_ signalClient: SignalingClient, didReceiveRemoteSdp sdp: RTCSessionDescription) {
        print("Received remote sdp")
        
        self.webRTCClient?.set(remoteSdp: sdp) { (error) in
            self.hasRemoteSdp = true
            self.RecievedCall()
        }
    }
    func RecievedCall() {
        var callIdentifier = ""
        switch callType {
        case .audio:
            callIdentifier = "Voice Call"
        case .video:
             callIdentifier  = "Video Call"
        case .chat:
             callIdentifier = "Messaging Chat Invitation"
        }
        
        let alert = UIAlertController.init(title: "Incoming Call", message: callIdentifier, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Accept", style: .default, handler: { (action) in
            self.answerDidTap(UIButton()) // answer a call
            switch callType {
            case .audio :
                self.performSegue(withIdentifier: "voicecall", sender: nil)
            case .video :
                self.videoDidTap(UIButton())
            case .chat:
                self.performSegue(withIdentifier: "chat", sender: nil)
            }
            
        }))
        alert.addAction(UIAlertAction(title: "Reject", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func signalClient(_ signalClient: SignalingClient, didReceiveCandidate candidate: RTCIceCandidate) {
        print("Received remote candidate")
        self.remoteCandidateCount += 1
        self.webRTCClient?.set(remoteCandidate: candidate)
    }
}

extension MainViewController: WebRTCClientDelegate {
    
    func webRTCClient(_ client: WebRTCClient, didDiscoverLocalCandidate candidate: RTCIceCandidate) {
        print("discovered local candidate")
        self.localCandidateCount += 1
        self.signalClient?.send(candidate: candidate)
    }
    
    func webRTCClient(_ client: WebRTCClient, didChangeConnectionState state: RTCIceConnectionState) {
        let textColor: UIColor
        switch state {
        case .connected, .completed:
            textColor = .green
            delegate?.callConnected()
        case .disconnected:
            textColor = .orange
            delegate?.callDisconnected()
            delegateForChat?.chatFinished()
        case .failed, .closed:
            textColor = .red
        case .new, .checking, .count:
            textColor = .black
        @unknown default:
            textColor = .black
        }
        DispatchQueue.main.async {
            self.webRTCStatusLabel?.text = state.description.capitalized
            self.webRTCStatusLabel?.textColor = textColor
        }
    }
    
    func webRTCClient(_ client: WebRTCClient, didReceiveData data: Data) {
        DispatchQueue.main.async {
            let message = String(data: data, encoding: .utf8) ?? "(Binary: \(data.count) bytes)"
            self.delegateForChat?.newMessageArrived(msg: message)
        }
    }
}

extension MainViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let dest = segue.destination as? VoiceCallViewController {
            dest.setRTC(webRTCClient: self.webRTCClient!)
            self.delegate = dest
        } else if let dest = segue.destination as? TextChatViewController {
            dest.setRTC(webRTCClient: self.webRTCClient!)
            self.delegateForChat = dest
           chatOffer()
        }
    }
    
    /// offer chat to reciever
    func chatOffer() {
        self.webRTCClient?.offer { (sdp) in
            self.hasLocalSdp = true
            self.signalClient?.senCallType(callT: CallType.chat)
            self.signalClient?.send(sdp: sdp)
        }
    }
    
    /// set top vc
    ///
    /// - Returns: top view controller
    func topMostController() -> UIViewController? {
        
        var presentedVC = UIApplication.shared.keyWindow?.rootViewController
        while let pVC = presentedVC?.presentedViewController {
            presentedVC = pVC
        }
        
        if presentedVC == nil {
            //print("AlertController Error: You don't have any views set. You may be calling in viewdidload. Try viewdidappear.")
        } else if let presentedNVC = presentedVC as? UINavigationController,
            let topVc = presentedNVC.topViewController {
            presentedVC = topVc
        }
        return presentedVC
    }
    
    @IBAction func messageTapped(_ sender : Any?) {
        performSegue(withIdentifier: "chat", sender: sender)
    }
    
}


protocol CallControlDelegate : NSObjectProtocol {
    func callConnected()
    func callDisconnected()
}

protocol ChatControlDelegate: NSObjectProtocol{
    func newMessageArrived(msg: String)
    func chatFinished()
}
