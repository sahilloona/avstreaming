//
//  VoiceCallViewController.swift
//  WebRTC-Demo
//
//  Created by Sahil Kumar on 19/06/19.
//  Copyright © 2019 Chicmic. All rights reserved.
//

import UIKit

class VoiceCallViewController: UIViewController {
    
     @IBOutlet private weak var muteButton: UIButton?
     @IBOutlet private weak var speakerButton: UIButton?
    @IBOutlet private weak var callStatusLabel : UILabel!
    
    private var webRTCClient: WebRTCClient?
    
    private var speakerOn: Bool = false {
        didSet {
            let title = "Speaker: \(self.speakerOn ? "On" : "Off" )"
            self.speakerButton?.setTitle(title, for: .normal)
        }
    }
    
    private var mute: Bool = false {
        didSet {
            let title = "Mute: \(self.mute ? "on" : "off")"
            self.muteButton?.setTitle(title, for: .normal)
        }
    }
   
    func setRTC(webRTCClient: WebRTCClient){
        self.webRTCClient = webRTCClient
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    self.speakerOn = false
        self.callStatusLabel?.text = "Connected"
        // Do any additional setup after loading the view.
    }
    
    @IBAction func declineCall (_ sender : Any?) {
        webRTCClient?.removeCandidate()
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction private func muteDidTap(_ sender: UIButton) {
        self.mute = !self.mute
        if self.mute {
            self.webRTCClient?.muteAudio()
        }
        else {
            self.webRTCClient?.unmuteAudio()
        }
    }
    @IBAction private func speakerDidTap(_ sender: UIButton) {
        if self.speakerOn {
            self.webRTCClient?.speakerOff()
        }
        else {
            self.webRTCClient?.speakerOn()
        }
        self.speakerOn = !self.speakerOn
    }

    
    func connectedCall() {
        DispatchQueue.main.async {
            self.callStatusLabel?.text = "Connected"
            self.callStatusLabel?.textColor = UIColor.green
            self.callStatusLabel?.setNeedsDisplay()
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension VoiceCallViewController:CallControlDelegate {
    func callConnected() {
        connectedCall()
        
        }
    
    func callDisconnected() {
        DispatchQueue.main.async {
            self.callStatusLabel?.text = "Disconnected"
            self.callStatusLabel?.textColor = UIColor.red
            self.dismiss(animated: true, completion: {[weak self] in
                self?.webRTCClient?.removeCandidate()
            })
        }
    }
    
    
}
