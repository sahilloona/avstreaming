//
//  Chat.swift
//  WebRTC-Demo
//
//  Created by Sahil Kumar on 20/06/19.
//  Copyright © 2019 Chicmic. All rights reserved.
//

import Foundation

struct Chat {
    var message : String
    var sender : Person
    
    init(message: String,sender:Person) {
        self.message = message
        self.sender = sender
    }
}
