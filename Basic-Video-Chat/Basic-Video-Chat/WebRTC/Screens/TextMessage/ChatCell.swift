//
//  ChatCell.swift
//  WebRTC-Demo
//
//  Created by Sahil Kumar on 20/06/19.
//  Copyright © 2019 Chicmic. All rights reserved.
//

import UIKit
enum Person {
    case me
    case other
}

class ChatCell: UITableViewCell {
    
    @IBOutlet weak var chatLabel : UILabel?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    /// to configure cell acccording to msg
    ///
    /// - Parameters:
    ///   - msg: message
    ///   - type: me or other
    func confiure(msg: String,type:Person) {
        chatLabel?.text = msg
        switch type {
        case .me:
            chatLabel?.textAlignment = .right
        case .other:
            chatLabel?.textAlignment = .left
        }
    }

}
