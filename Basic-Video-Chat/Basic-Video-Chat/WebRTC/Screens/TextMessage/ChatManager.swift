//
//  ChatManager.swift
//  WebRTC-Demo
//
//  Created by Sahil Kumar on 20/06/19.
//  Copyright © 2019 Chicmic. All rights reserved.
//

import Foundation

class ChatManager {
    private var chat = [Chat]()
    
    static let shared = ChatManager()
    
    
    /// for new message arrived
    ///
    /// - Parameter msg: new message arrived
    func newMessage(msg: String,sender: Person) {
        self.chat.append(Chat.init(message: msg, sender: sender))
    }
    
    /// to get whole chat saved in cache
    ///
    /// - Returns: return whole chat
    func getChat()->[Chat] {
        return self.chat
    }
    
    /// to get chat count
    ///
    /// - Returns: return count of chat
    func getChatCount()->Int {
        return self.chat.count
    }
    
    
    func getChat(at index : Int)->Chat {
        return chat[index]
    }
}
