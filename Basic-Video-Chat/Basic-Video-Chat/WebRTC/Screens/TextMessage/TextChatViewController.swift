//
//  TextChatViewController.swift
//  WebRTC-Demo
//
//  Created by Sahil Kumar on 20/06/19.
//  Copyright © 2019 Chicmic. All rights reserved.
//

import UIKit

class TextChatViewController: UIViewController {
    
    @IBOutlet weak var tableView : UITableView?
    
    private weak var webRTCClient : WebRTCClient?
    
    @IBOutlet weak var textLabel : UITextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupForChat()
    }
    
    func setupForChat() {
        webRTCClient?.muteAudio()
    }
    
    func setRTC(webRTCClient:  WebRTCClient) {
        self.webRTCClient = webRTCClient
    }
    
    
    /// to send message
    ///
    /// - Parameter sender: button
    @IBAction func sendMessage(_ sender: Any?) {
        textLabel?.resignFirstResponder()
        if let msg = textLabel?.text {
            ChatManager.shared.newMessage(msg: msg, sender: .me)
            self.sendMessageAsData(msg: msg)
            textLabel?.text = ""
            tableView?.reloadData()
        }
    }
    @IBAction func exitBtnTapped(_ sender: Any?) {
        self.dismiss(animated: true) {
            self.webRTCClient?.removeCandidate()
        }
    }
    
}

extension TextChatViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ChatManager.shared.getChatCount()
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let chat = ChatManager.shared
        
       if let cell = tableView.dequeueReusableCell(withIdentifier: "chat") as? ChatCell {
        let msg = chat.getChat(at: indexPath.row)
        cell.confiure(msg: msg.message, type: msg.sender)
        
        return cell
       } else {
        return UITableViewCell()
    }
        
    }
}

extension TextChatViewController {
    
    func sendMessageAsData(msg: String) {
        guard let dataToSend = msg.data(using: .utf8) else {
            return
        }
        self.webRTCClient?.sendData(dataToSend)
    }
}

extension TextChatViewController: ChatControlDelegate {
    
    /// delegate called when new message will arrive
    ///
    /// - Parameter msg: message
    func newMessageArrived(msg: String) {
        DispatchQueue.main.async {[weak self] in
            ChatManager.shared.newMessage(msg: msg, sender: .other)
            self?.tableView?.reloadData()
        }
    }
    
    func chatFinished() {
        let alert = UIAlertController.init(title: "", message: "Person Left", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
}

extension TextChatViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
   
}
