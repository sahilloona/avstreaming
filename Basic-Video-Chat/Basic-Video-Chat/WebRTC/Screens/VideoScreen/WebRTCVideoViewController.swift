//
//  VideoViewController.swift
//  WebRTC
//
//  Created by Chicmic on 21/05/2018.
//  Copyright © 2018 Chicmic. All rights reserved.
//

import UIKit
import WebRTC

class WebRTCVideoViewController: UIViewController {

    @IBOutlet private weak var localVideoView: UIView?
    private weak var webRTCClient: WebRTCClient?
//    private var localRenderer :RTCMTLVideoView?
//    private var remoteRenderer :RTCMTLVideoView?

    init(webRTCClient: WebRTCClient) {
        self.webRTCClient = webRTCClient
        super.init(nibName: String(describing: VideoViewController.self), bundle: Bundle.main)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        #if arch(arm64)
            // Using metal (arm64 only)
           let localRenderer = RTCMTLVideoView(frame: self.localVideoView?.frame ?? CGRect.zero)
           let remoteRenderer = RTCMTLVideoView(frame: self.view.frame)
            localRenderer.videoContentMode = .scaleAspectFill
            remoteRenderer.videoContentMode = .scaleAspectFill
        #else
            // Using OpenGLES for the rest
            let localRenderer = RTCEAGLVideoView(frame: self.localVideoView?.frame ?? CGRect.zero)
            let remoteRenderer = RTCEAGLVideoView(frame: self.view.frame)
        #endif

        self.webRTCClient?.startCaptureLocalVideo(renderer: localRenderer)
        self.webRTCClient?.renderRemoteVideo(to: remoteRenderer)
        
        if let localVideoView = self.localVideoView {
            self.embedView(localRenderer, into: localVideoView)
        }
        self.embedView(remoteRenderer, into: self.view)
        self.view.sendSubviewToBack(remoteRenderer)
        
        self.webRTCClient?.speakerOn()
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    private func embedView(_ view: UIView, into containerView: UIView) {
        containerView.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        containerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[view]|",
                                                                    options: [],
                                                                    metrics: nil,
                                                                    views: ["view":view]))
        
        containerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[view]|",
                                                                    options: [],
                                                                    metrics: nil,
                                                                    views: ["view":view]))
        containerView.layoutIfNeeded()
    }
    
    @IBAction private func backDidTap(_ sender: Any) {
//        if let local = localRenderer,let remote = remoteRenderer {
//            webRTCClient.removeVideoTrack(localRenderer: local, remoteRenderer: remote)
//        }
        webRTCClient?.removeCandidate()
        
       
            self.dismiss(animated: true)
    }
}

extension WebRTCVideoViewController : CallControlDelegate {
    func callConnected() {
    self.webRTCClient?.speakerOn()
    }
    
    func callDisconnected() {
        self.dismiss(animated: true) {[weak self] in
            self?.webRTCClient?.removeCandidate()
        }
        
    }
    func delay(_ seconds: Double, completion: @escaping () -> ()) {
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            completion()
        }
    }
    
}
