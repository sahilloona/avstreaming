//
//  Utility.swift
//  WebRTC-Demo
//
//  Created by Sahil Kumar on 21/06/19.
//  Copyright © 2019 Chicmic. All rights reserved.
//

import Foundation

class  RTCUtlilty {
    
    /// to perform action asynchronusly after some delay
    ///
    /// - Parameters:
    ///   - seconds: seconds
    ///   - completion: action to be performed
    class func delay(_ seconds: Double, completion: @escaping () -> ()) {
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            completion()
        }
    }
}
