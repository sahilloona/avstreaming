//
//  ChooseAVVC.swift
//  Basic-Video-Chat
//
//  Created by Chicmic on 07/05/19.
//  Copyright © 2019 tokbox. All rights reserved.
//

import UIKit

class ChooseAVVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    //MARK: - Prepare Methods

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == WowzaSegueIdentifier.audioStreaming {
            let audioStreamingDest = segue.destination as? VideoViewController
            audioStreamingDest?.isStreamingFor = IsSettingFor.AudioStreaming
        } else if segue.identifier == WowzaSegueIdentifier.videoStreaming {
            let videoStreamingDest = segue.destination as? VideoViewController
            videoStreamingDest?.isStreamingFor = IsSettingFor.VideoStreaming
        }
    }
    
}
