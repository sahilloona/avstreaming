//
//  settings.swift
//  Basic-Video-Chat
//
//  Created by Chicmic on 06/05/19.
//  Copyright © 2019 tokbox. All rights reserved.
//

import Foundation

class Setting {
    var title: String = ""
    var value: String = ""
    var type: SettingType = .hostAddress
    
    init(title: String, value: String, type: SettingType ) {
        self.title = title
        self.value = value
        self.type = type
    }
    
    // MARK: - Methods
    
    static func getSettingData(_ isForPlayer: IsSettingFor) -> [Setting] {
        let hostAddress = Setting(title: "Host Address", value: "", type: .hostAddress)
        let portNumber = Setting(title: "Port Number", value: "", type: .portNumber)
        let applicationName = Setting(title: "Application Name", value: "", type: .applicationName)
        let streamName = Setting(title: "Stream Name", value: "", type: .streamName)
        let preRollDuration = Setting(title: "PreRoll Duration", value: "", type: .preRollDuration)
        let hlsURL = Setting(title: "HLSURL", value: "", type: .hlsURL)
        let username = Setting(title: "Username", value: "", type: .username)
        let password = Setting(title: "Password", value: "", type: .password)
        let videoFrameRate = Setting(title: "Video Frame Rate", value: "", type: .videoFrameRate)
        
        if isForPlayer == .wowzaPlayer {
            return [hostAddress,portNumber,applicationName,streamName,preRollDuration,
                    username,password,hlsURL]
        } else {
            return [hostAddress,portNumber,applicationName,streamName,videoFrameRate,
                    username,password]
        }
    }
}
