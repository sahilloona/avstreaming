//
//  SettingViewCell.swift
//  Basic-Video-Chat
//
//  Created by Chicmic on 06/05/19.
//  Copyright © 2019 tokbox. All rights reserved.
//

import UIKit

class SettingViewCell: UITableViewCell{

    @IBOutlet weak var cellTitle: UILabel!
    @IBOutlet weak var cellTextField: UITextField!
    private var settingObj: Setting?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
       
    }
    
    // MARK: - UI Action Methods
    
    @IBAction func editingChange(_ sender: Any) {
        self.settingObj?.value = cellTextField.text ?? ""
    }
    
    // MARK: - Methods
    
    func configureCell(setting: Setting) {
        settingObj = setting
        cellTitle.text = settingObj?.title
        if setting.title == AppConstant.securePassword {
            cellTextField.isSecureTextEntry = true
        }
    }
}
