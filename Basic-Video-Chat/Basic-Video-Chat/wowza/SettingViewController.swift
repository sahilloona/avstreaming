//
//  SettingViewController.swift
//  Basic-Video-Chat
//
//  Created by Chicmic on 06/05/19.
//  Copyright © 2019 tokbox. All rights reserved.
//

import UIKit
import CoreData

class SettingViewController: UITableViewController {

    private var settingList: [Setting] = []
    var isSettingFor: IsSettingFor?
    var isStreamingFor: IsSettingFor?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isSettingFor == .wowzaPlayer {
            isSettingFor = .wowzaPlayer
        } else {
            isSettingFor = .wowzaLiveStreaming
        }
        guard let isSettingFor = isSettingFor else {
            return
        }
        settingList = Setting.getSettingData(isSettingFor)
    }
    
    //MARK - UI Action Methods
    
    @IBAction func closeButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneButton(_ sender: Any) {
            createData()
        retrieveData()
        if (WowzaConfigure.hostAddress != nil), (WowzaConfigure.portNumber != nil), (WowzaConfigure.applicationName != nil), (WowzaConfigure.streamName != nil), (WowzaConfigure.username != nil), (WowzaConfigure.password != nil) {
            if isSettingFor == IsSettingFor.wowzaLiveStreaming {
                if (WowzaAdditionalConfi.videoFrameRate != nil && isSettingFor == IsSettingFor.wowzaLiveStreaming) {
                    self.dismiss(animated: true, completion: nil)
                } else {
                    let alert = UIAlertController.init(title: Alert.alertTitle, message: Alert.textFieldError, preferredStyle: .alert)
                    alert.addAction(UIAlertAction.init(title: Alert.buttonTitle, style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            } else if (isSettingFor == IsSettingFor.wowzaPlayer){
                if (WowzaAdditionalConfi.hlsURL != nil && isSettingFor == IsSettingFor.wowzaPlayer), (WowzaAdditionalConfi.preRollDuration != nil && isSettingFor == IsSettingFor.wowzaPlayer){
                    createData()
                    self.dismiss(animated: true, completion: nil)
                }
                else {
                    let alert = UIAlertController.init(title: Alert.alertTitle, message: Alert.textFieldError, preferredStyle: .alert)
                    alert.addAction(UIAlertAction.init(title: Alert.buttonTitle, style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        } else {
            let alert = UIAlertController.init(title: Alert.alertTitle, message: Alert.textFieldError, preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: Alert.buttonTitle, style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return settingList.count
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return AppConstant.configureWowza
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AppConstant.settingCell, for: indexPath) as! SettingViewCell
        
        cell.configureCell(setting: settingList[indexPath.row])
        
        return cell
    }
    
    // MARK: - Methods
    
    private func createData() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let userEntity = NSEntityDescription.entity(forEntityName: AppConstant.entityWowzaConf, in: managedContext)!
        
        let wowzaConfig = NSManagedObject(entity: userEntity, insertInto: managedContext)
        
        settingList.forEach { (setting) in
            switch (setting.type) {
            case .hostAddress:
                wowzaConfig.setValue(setting.value ,forKey: AppConstant.hostAddress)
            case .portNumber:
                wowzaConfig.setValue(Int16(setting.value) ,forKey: AppConstant.portNumber)
            case .applicationName:
                wowzaConfig.setValue(setting.value ,forKey: AppConstant.applicationName)
            case .streamName:
                wowzaConfig.setValue(setting.value ,forKey: AppConstant.streamName)
            case .password:
                wowzaConfig.setValue(setting.value ,forKey: AppConstant.password)
            case .username:
                wowzaConfig.setValue(setting.value ,forKey: AppConstant.username)
            case .hlsURL where isSettingFor == IsSettingFor.wowzaPlayer:
                wowzaConfig.setValue(setting.value ,forKey: AppConstant.hlsURL)
            case .videoFrameRate where isSettingFor == IsSettingFor.wowzaLiveStreaming:
                wowzaConfig.setValue(Int16(setting.value) ,forKey: AppConstant.videoFrameRate)
            case .preRollDuration where isSettingFor == IsSettingFor.wowzaPlayer:
                wowzaConfig.setValue(Float64(setting.value) ,forKey: AppConstant.preRollDuration)
            default:
                break
            }
        }
        do {
            try managedContext.save()
            
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    private func retrieveData() {
        
        //As we know that container is set up in the AppDelegates so we need to refer that container.
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        //We need to create a context from this container
        let managedContext = appDelegate.persistentContainer.viewContext
        
        //Prepare the request of type NSFetchRequest  for the entity
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: AppConstant.entityWowzaConf)

        do {
            let result = try managedContext.fetch(fetchRequest)
            for data in result as! [NSManagedObject] {
                WowzaConfigure.hostAddress = data.value(forKey: AppConstant.hostAddress) as? String
                WowzaConfigure.portNumber = data.value(forKey: AppConstant.portNumber) as? UInt
                WowzaConfigure.streamName = data.value(forKey: AppConstant.streamName) as? String
                WowzaConfigure.applicationName = data.value(forKey: AppConstant.applicationName) as? String
                WowzaConfigure.username = data.value(forKey: AppConstant.username) as? String
                WowzaConfigure.password = data.value(forKey: AppConstant.password) as? String
                
                WowzaAdditionalConfi.hlsURL = data.value(forKey: AppConstant.hlsURL) as? String
                WowzaAdditionalConfi.preRollDuration = data.value(forKey: AppConstant.preRollDuration) as? Float64
                WowzaAdditionalConfi.videoFrameRate = data.value(forKey: AppConstant.videoFrameRate) as? UInt
            }
            
        } catch {
            print("Failed")
        }
    }
}
