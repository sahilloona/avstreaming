//
//  WowzaIncomingCallVC.swift
//  Basic-Video-Chat
//
//  Created by Chicmic on 01/05/19.
//  Copyright © 2019 tokbox. All rights reserved.
//


import UIKit
import WowzaGoCoderSDK

class VideoViewController: UIViewController {
    
    //MARK: - Class Member Variables
    
    @IBOutlet weak var broadcastButton:UIButton!
    @IBOutlet weak var switchCameraButton:UIButton!
    @IBOutlet weak var torchButton:UIButton!
    @IBOutlet weak var micButton:UIButton!
    @IBOutlet weak var closeButton:UIButton!
    
    private var goCoder:WowzaGoCoder?
    private var goCoderConfig:WowzaConfig!
    var isStreamingFor: IsSettingFor?
    private var receivedGoCoderEventCodes = Array<WOWZEvent>()
    private var blackAndWhiteVideoEffect = false
    private var bitmapOverlayVideoEffect = false
    private var goCoderRegistrationChecked = false
    private var goCoderLicence: Error?
    
    //MARK: - Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isStreamingFor == IsSettingFor.AudioStreaming {
            
        } else if isStreamingFor == IsSettingFor.VideoStreaming {
            
        }
        let goCoderLicensingError = WowzaGoCoder.registerLicenseKey(AppConstant.sDKSampleAppLicenseKey)
        if goCoderLicensingError != nil {
            print(goCoderLicensingError?.localizedDescription)
        } else {
            goCoder = WowzaGoCoder.sharedInstance()
        }
        if self.goCoder != nil{
            configBroadcast()
            goCoderLicence = goCoderLicensingError
        }
        
        // Reload any saved data
        blackAndWhiteVideoEffect = UserDefaults.standard.bool(forKey: AppConstant.blackAndWhiteEffectKey)
        bitmapOverlayVideoEffect = UserDefaults.standard.bool(forKey: AppConstant.bitmapOverlayEffectKey)
        WowzaGoCoder.setLogLevel(.default)
        
        if let goCoderLicensingError = goCoderLicensingError {
            self.showAlert(Alert.sdkLicense, error: goCoderLicensingError as NSError)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configBroadcast()
        // Update the configuration settings in the GoCoder
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if isStreamingFor == IsSettingFor.AudioStreaming {
            
        } else if isStreamingFor == IsSettingFor.VideoStreaming {
            goCoder?.cameraPreview?.previewLayer?.frame = view.bounds
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if !goCoderRegistrationChecked {
            if (goCoderLicence != nil) {
                self.showAlert(Alert.sdkLicense, error: goCoderLicence as! NSError)
            }
            else {
                goCoderRegistrationChecked = true
                
                // Initialize the GoCoder SDK
                if let goCoder = WowzaGoCoder.sharedInstance() {
                    self.goCoder = goCoder
                    //issue
                    self.goCoder?.register(self as WOWZVideoSink)
                    self.goCoder?.register(self as WOWZAudioSink)
                    self.goCoder?.config = self.goCoderConfig
                    
                    // Specify the view in which to display the camera preview
                    if isStreamingFor == IsSettingFor.AudioStreaming {
                        
                    } else if isStreamingFor == IsSettingFor.VideoStreaming {
                        self.goCoder?.cameraView = self.view
                        self.goCoder?.cameraPreview?.start()
                    }
                    // Start the camera preview
                    //closeButton.isEnabled = true
                }
                self.updateUIControls()
            }
        }
        else{
            // Start the camera previewself.
            if isStreamingFor == IsSettingFor.AudioStreaming {
                
            } else if isStreamingFor == IsSettingFor.VideoStreaming {
                self.goCoder?.cameraPreview?.start()
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if isStreamingFor == IsSettingFor.AudioStreaming {
            
        } else if isStreamingFor == IsSettingFor.VideoStreaming {
            self.goCoder?.cameraPreview?.stop()
        }
    }
    
    override var prefersStatusBarHidden:Bool {
        return true
    }
    
    //MARK - UI Action Methods
    
    @IBAction func didTapBroadcastButton(_ sender:AnyObject?) {
        // Ensure the minimum set of configuration settings have been specified necessary to
        // initiate a broadcast streaming session
        if let configError = goCoder?.config.validateForBroadcast() {
            self.showAlert(Alert.incompleteStreamingSetting, error: configError as NSError)
        }
        else {
            // Disable the U/I controls
            broadcastButton.isEnabled    = false
            if isStreamingFor == IsSettingFor.AudioStreaming {
                
            } else if isStreamingFor == IsSettingFor.VideoStreaming {
                torchButton.isEnabled        = false
                switchCameraButton.isEnabled = false
            }
            if goCoder?.status.state == .running {
                goCoder?.endStreaming(self)
            } else {
                receivedGoCoderEventCodes.removeAll()
                goCoder?.startStreaming(self)
                let audioMuted = goCoder?.isAudioMuted ?? false
                micButton.setImage(UIImage(named: audioMuted ? ButtonImage.micOFF : ButtonImage.micON), for: UIControl.State())
            }
        }
    }
    
    @IBAction func didTapSwitchCameraButton(_ sender:AnyObject?) {
        if let otherCamera = goCoder?.cameraPreview?.otherCamera() {
            if !otherCamera.supportsWidth(goCoderConfig.videoWidth) {
                goCoderConfig.load(otherCamera.supportedPresetConfigs.last!.toPreset())
                goCoder?.config = goCoderConfig
            }
            
            goCoder?.cameraPreview?.switchCamera()
            torchButton.setImage(UIImage(named: ButtonImage.torchON), for: UIControl.State())
            self.updateUIControls()
        }
    }
    
    @IBAction func didTapTorchButton(_ sender:AnyObject?) {
        var newTorchState = goCoder?.cameraPreview?.camera?.isTorchOn ?? true
        newTorchState = !newTorchState
        goCoder?.cameraPreview?.camera?.isTorchOn = newTorchState
        torchButton.setImage(UIImage(named: newTorchState ? ButtonImage.torchOFF : ButtonImage.torchON), for: UIControl.State())
    }
    
    @IBAction func didTapMicButton(_ sender:AnyObject?) {
        var newMutedState = self.goCoder?.isAudioMuted ?? true
        newMutedState = !newMutedState
        goCoder?.isAudioMuted = newMutedState
        micButton.setImage(UIImage(named: newMutedState ? ButtonImage.micOFF : ButtonImage.micON), for: UIControl.State())
    }
    
    @IBAction func close(_ sender: AnyObject?) {
        self.dismiss(animated: true, completion: nil)
        self.goCoder?.unregisterAudioSink(self as WOWZAudioSink)
        self.goCoder?.unregisterVideoSink(self as WOWZVideoSink)
    }
    
    //MARK - Methods
    
    private func configBroadcast() {
        guard let config = self.goCoder?.config else {
            return
        }
        let goCoderBroadcastConfig: WowzaConfig = config
        goCoderBroadcastConfig.load(WOWZFrameSizePreset.preset1280x720)
        goCoderBroadcastConfig.hostAddress = WowzaConfigure.hostAddress
        goCoderBroadcastConfig.portNumber = WowzaConfigure.portNumber ?? 1935
        goCoderBroadcastConfig.applicationName = WowzaConfigure.applicationName
        goCoderBroadcastConfig.streamName = WowzaConfigure.streamName
        
        goCoderBroadcastConfig.username = WowzaConfigure.username
        goCoderBroadcastConfig.password = WowzaConfigure.password
        goCoderBroadcastConfig.videoFrameRate = WowzaAdditionalConfi.videoFrameRate ?? 30
        
        goCoderBroadcastConfig.audioEnabled = true
        if isStreamingFor == IsSettingFor.AudioStreaming {
            goCoderBroadcastConfig.videoEnabled = false
        } else if isStreamingFor == IsSettingFor.VideoStreaming {
            goCoderBroadcastConfig.videoEnabled = true
        }
        self.goCoderConfig = goCoderBroadcastConfig
    }
    
    private func updateUIControls() {
        if self.goCoder?.status.state != .idle && self.goCoder?.status.state != .running {
            // If a streaming broadcast session is in the process of starting up or shutting down,
            // disable the UI controls
            self.broadcastButton.isEnabled = false
            if isStreamingFor == IsSettingFor.AudioStreaming {
                
            } else if isStreamingFor == IsSettingFor.VideoStreaming {
                self.torchButton.isEnabled = false
                self.switchCameraButton.isEnabled = false
            }
            self.closeButton.isEnabled = true
            self.micButton.isHidden = true
            self.micButton.isEnabled = false
        } else {
            // Set the UI control state based on the streaming broadcast status, configuration,
            // and device capability
            self.broadcastButton.isEnabled = true
            if isStreamingFor == IsSettingFor.AudioStreaming {
                
            } else if isStreamingFor == IsSettingFor.VideoStreaming {
                self.switchCameraButton.isEnabled = ((self.goCoder?.cameraPreview?.cameras?.count) ?? 0) > 1
                self.torchButton.isEnabled = self.goCoder?.cameraPreview?.camera?.hasTorch ?? false
            }
            let isStreaming = self.goCoder?.isStreaming ?? false
            self.micButton.isEnabled = isStreaming && self.goCoderConfig.audioEnabled
            if isStreamingFor == IsSettingFor.AudioStreaming {
                
            } else if isStreamingFor == IsSettingFor.VideoStreaming {
                self.micButton.isHidden = !self.micButton.isEnabled
            }
        }
    }
    
    //MARK: - Alerts
    
    private func showAlert(_ title:String, status:WOWZStatus) {
        let alertController = UIAlertController(title: title, message: status.description, preferredStyle: .alert)
        
        let action = UIAlertAction(title: Alert.alertTitle, style: .default, handler: nil)
        alertController.addAction(action)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    private func showAlert(_ title:String, error:NSError) {
        let alertController = UIAlertController(title: title, message: error.localizedDescription, preferredStyle: .alert)
        
        let action = UIAlertAction(title: Alert.alertTitle, style: .default, handler: nil)
        alertController.addAction(action)
        
        self.present(alertController, animated: true, completion: nil)
    }
    //MARK - Prepare Methods
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == WowzaSegueIdentifier.Listening {
            let dest = segue.destination as? WowzaIncomingCallVC
            dest?.isStreamingFor = isStreamingFor
        }
    }
    
}

extension VideoViewController: WOWZStatusCallback, WOWZVideoSink, WOWZAudioSink {
    
    //MARK: - WOWZStatusCallback Protocol Instance Methods
    
    func onWOWZStatus(_ status: WOWZStatus!) {
        switch (status.state) {
        case .idle:
            DispatchQueue.main.async { () -> Void in
                if self.isStreamingFor == IsSettingFor.AudioStreaming {
                    self.broadcastButton.setImage(UIImage(named: ButtonImage.startRecording), for: UIControl.State())
                } else if self.isStreamingFor == IsSettingFor.VideoStreaming {
                    self.broadcastButton.setImage(UIImage(named: ButtonImage.startVideo), for: UIControl.State())
                }
                
                self.updateUIControls()
            }
            
        case .running:
            DispatchQueue.main.async { () -> Void in
                if self.isStreamingFor == IsSettingFor.AudioStreaming {
                    self.broadcastButton.setImage(UIImage(named: ButtonImage.stopRecording), for: UIControl.State())
                } else if self.isStreamingFor == IsSettingFor.VideoStreaming {
                    self.broadcastButton.setImage(UIImage(named: ButtonImage.stopVideo), for: UIControl.State())
                }
                self.updateUIControls()
            }
        case .stopping, .starting:
            DispatchQueue.main.async { () -> Void in
                self.updateUIControls()
            }
            
        case .buffering: break
        default: break
        }
    }
    
    func onWOWZEvent(_ status: WOWZStatus!) {
        // If an event is reported by the GoCoder SDK, display an alert dialog describing the event,
        // but only if we haven't already shown an alert for this event
        DispatchQueue.main.async { () -> Void in
            if !self.receivedGoCoderEventCodes.contains(status.event) {
                self.receivedGoCoderEventCodes.append(status.event)
                self.showAlert(Alert.liveStreamEvent, status: status)
            }
            self.updateUIControls()
        }
    }
    
    func onWOWZError(_ status: WOWZStatus!) {
        // If an error is reported by the GoCoder SDK, display an alert dialog containing the error details
        DispatchQueue.main.async { () -> Void in
            self.showAlert(Alert.liveStreamError, status: status)
            self.updateUIControls()
        }
    }
    
    //MARK: - WOWZZVideoSink Protocol Methods
    
    func videoFrameWasCaptured(_ imageBuffer: CVImageBuffer, framePresentationTime: CMTime, frameDuration: CMTime) {
        if goCoder != nil && goCoder!.isStreaming && blackAndWhiteVideoEffect {
            // convert frame to b/w using CoreImage tonal filter
            var frameImage = CIImage(cvImageBuffer: imageBuffer)
            if let grayFilter = CIFilter(name: AppConstant.cIPhotoEffectTonal) {
                grayFilter.setValue(frameImage, forKeyPath: AppConstant.inputImage)
                if let outImage = grayFilter.outputImage {
                    frameImage = outImage
                    
                    let context = CIContext(options: nil)
                    context.render(frameImage, to: imageBuffer)
                }
            }
        }
        
        if goCoder != nil && goCoder!.isStreaming && bitmapOverlayVideoEffect {
            //            let wowzOverlayImg = bitmapOverlayImgView.image?.ciImage;
            CVPixelBufferLockBaseAddress(imageBuffer, []);
            let context = CGContext(data: CVPixelBufferGetBaseAddress(imageBuffer),
                                    width: CVPixelBufferGetWidth(imageBuffer),
                                    height: CVPixelBufferGetHeight(imageBuffer),
                                    bitsPerComponent: 8,
                                    bytesPerRow: CVPixelBufferGetBytesPerRow(imageBuffer),
                                    space: CGColorSpaceCreateDeviceRGB(),
                                    bitmapInfo: CGBitmapInfo.byteOrder32Little.rawValue | CGImageAlphaInfo.premultipliedFirst.rawValue );
            let height = goCoderConfig.videoHeight+100; /// accommodating for header / footer areas
            CVPixelBufferUnlockBaseAddress(imageBuffer, []);
        }
    }
    
    func videoCaptureInterruptionStarted() {
        goCoder?.endStreaming(self)
    }
    
    //MARK: - WOWZAudioSink Protocol Methods
    
    func audioLevelDidChange(_ level: Float) {
        //        print("Audio level did change: \(level)");
    }
}
