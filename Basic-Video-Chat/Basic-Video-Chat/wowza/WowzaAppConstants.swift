//
//  WowzaAppConstants.swift
//  Basic-Video-Chat
//
//  Created by Chicmic on 06/05/19.
//  Copyright © 2019 tokbox. All rights reserved.
//

import Foundation

enum SettingType {
    case hostAddress, portNumber,
    applicationName,streamName,
    preRollDuration, hlsURL,
    username, password,
    videoFrameRate;
}

enum IsSettingFor {
    case wowzaLiveStreaming, wowzaPlayer, AudioStreaming, VideoStreaming
}

struct ButtonImage {
    static let micOFF = "mic_off_button"
    static let micON = "mic_on_button"
    static let torchON = "torch_on_button"
    static let torchOFF = "torch_off_button"
    static let startVideo = "start_button"
    static let stopVideo = "stop_button"
    static let startRecording = "startRecording"
    static let stopRecording = "sotpRecording"
    static let startPlayback = "playback_button"
    static let CallUp = "CallUp"
}

struct StatusMessage {
    static let broadcast = "Broadcast Message:"
    static let idle = "The broadcast is stopped"
    static let starting = "Broadcast initialization"
    static let running = "Streaming is active"
    static let stoping = "Broadcast shutting down"
    static let buffering = "The broadcasting component or session is buffering"
    static let ready = "The broadcasting component or session is ready or has been initialized"
}

struct WowzaSegueIdentifier{
    static let wowzaPlayerSetting = "WowzaPlayerSetting"
    static let audioStreaming = "ChooseToAudio"
    static let videoStreaming = "ChooseToVideo"
    static let Listening = "Listening"
}

struct Alert {
    static let alertTitle = "Alert"
    static let buttonTitle = "Ok"
    static let sdkLicense = "GoCoder SDK Licensing Error"
    static let incompleteStreamingSetting = "Incomplete Streaming Settings"
    static let liveStreamEvent = "Live Streaming Event"
    static let liveStreamError = "Live Streaming Error"
    static let textFieldError = "Fill All The Field And Fill Them Correct"
}
struct AppConstant {
    static let sDKSampleAppLicenseKey = "GOSK-6846-010C-90EE-8E08-1FBD"
    static let blackAndWhiteEffectKey = "BlackAndWhiteKey"
    static let bitmapOverlayEffectKey = "BitmapOverlayKey"
    static let cIPhotoEffectTonal = "CIPhotoEffectTonal"
    static let inputImage = "inputImage"
    static let connecting = "Connecting..."
    static let hlsconnecting = "Connecting to HLS fallback..."
    static let chicmic = "Chicmic"
    static let configureWowza = "Configure Wowza"
    static let entityWowzaConf = "WowzaConf"
    static let hostAddress = "hostAddress"
    static let portNumber = "portNumber"
    static let applicationName = "applicationName"
    static let streamName = "streamName"
    static let password = "password"
    static let username = "username"
    static let hlsURL = "hlsURL"
    static let videoFrameRate = "videoFrameRate"
    static let preRollDuration = "preRollDuration"
    static let settingCell = "SettingCell"
    static let securePassword = "Password"
}

