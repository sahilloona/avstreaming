//
//  WowzaIncomingCallVC.swift
//  Basic-Video-Chat
//
//  Created by Chicmic on 01/05/19.
//  Copyright © 2019 tokbox. All rights reserved.
//


import UIKit
import AVKit
import WowzaGoCoderSDK

class WowzaIncomingCallVC: UIViewController,WOWZStatusCallback,WOWZDataSink {
    
    @IBOutlet weak var volumeSlider: UISlider!
    @IBOutlet weak var playBackSlider: UISlider!
    @IBOutlet weak var playerView: UIView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var playPauseButton: UIButton!
    @IBOutlet weak var muteButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    
    
    private var wowzaPlayer = WOWZPlayer()
    private var goCoderConfig: WowzaConfig?
    var isStreamingFor: IsSettingFor?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        WowzaGoCoder.setLogLevel(WowzaGoCoderLogLevel.default)
        UIApplication.shared.isIdleTimerDisabled = true
        let goCoderLicensingError = WowzaGoCoder.registerLicenseKey(AppConstant.sDKSampleAppLicenseKey)
        if goCoderLicensingError != nil {
            print(goCoderLicensingError?.localizedDescription)
        } else {
            configueGoCoder()
            configWowzaPlayer()
        }
        goCoderConfig?.audioEnabled = true
        if isStreamingFor == IsSettingFor.AudioStreaming {
            goCoderConfig?.videoEnabled = false
        } else if isStreamingFor == IsSettingFor.VideoStreaming {
            goCoderConfig?.videoEnabled = true
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        configueGoCoder()
        self.goCoderConfig?.allowHLSPlayback = true
        self.goCoderConfig?.hlsURL = WowzaAdditionalConfi.hlsURL as NSString?
        self.wowzaPlayer.resetPlaybackErrorCount()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        wowzaPlayer.updateViewLayouts()
    }
    
    //MARK - UI Action Methods
    
    @IBAction func didTapAspectButton(_ senderId: Any){
        if(self.wowzaPlayer.playerViewGravity == WOWZPlayerViewGravity.resizeAspectFill){
            self.wowzaPlayer.playerViewGravity = WOWZPlayerViewGravity.resizeAspect;
        }else{
            self.wowzaPlayer.playerViewGravity = WOWZPlayerViewGravity.resizeAspectFill;
        }
    }
    
    @IBAction func didTapPlayPauseButton(_ senderId: Any){
        self.playPauseButton.isSelected = !self.playPauseButton.isSelected
        if let goCoderConfig = self.goCoderConfig, self.wowzaPlayer.currentPlayState() == WOWZState.idle, self.playPauseButton.isSelected == true {
            if self.wowzaPlayer.currentPlaybackErrorCount() > 2 {
                self.infoLabel.text = AppConstant.hlsconnecting;
            } else {
                self.infoLabel.text = AppConstant.connecting;
            }
            
            self.infoLabel.isHidden = false;
            self.infoLabel.alpha = 1;
            self.playPauseButton.isEnabled = false;
            // issue
            self.wowzaPlayer.play(goCoderConfig, callback: self)
        } else {
            self.wowzaPlayer.resetPlaybackErrorCount()
            self.wowzaPlayer.stop()
            self.infoLabel.isEnabled = false
        }
    }
    
    @IBAction func didChangeVolume(_ senderId: Any){
        self.wowzaPlayer.volume = volumeSlider.value
    }
    
    @IBAction func muteButton(_ senderId: Any){
        self.muteButton.isSelected = !self.muteButton.isSelected
        self.wowzaPlayer.muted = !self.wowzaPlayer.muted
        self.volumeSlider.isEnabled = !self.wowzaPlayer.muted
    }
    
    @IBAction func playBack(_ sender: Any) {
        self.wowzaPlayer.syncOffset = playBackSlider.value
    }
    
    @IBAction func settingButton(_ sender: Any) {
        
    }
    
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - Methods

    private func configueGoCoder() {
        let goCorderConfigure = WowzaConfig()
        goCorderConfigure.hostAddress = WowzaConfigure.hostAddress
        goCorderConfigure.portNumber = WowzaConfigure.portNumber ?? 1935
        goCorderConfigure.streamName = WowzaConfigure.streamName
        goCorderConfigure.applicationName = WowzaConfigure.applicationName
        goCorderConfigure.videoFrameRate = WowzaAdditionalConfi.videoFrameRate ?? 30
        
        goCorderConfigure.username = WowzaConfigure.username
        goCorderConfigure.password = WowzaConfigure.password
        
        self.goCoderConfig = goCorderConfigure
    }
    
    private func configWowzaPlayer() {
        wowzaPlayer.prerollDuration = 3.0// WowzaAdditionalConfi.preRollDuration ?? 3.0
        wowzaPlayer.register(self as WOWZDataSink, eventName: AppConstant.chicmic)
    }
    
    //MARK: - WOWZ Protocol Instance Methods
    
    func onData(_ dataEvent: WOWZDataEvent) {
        //nothing to done
    }
    
    func onWOWZStatus(_ status: WOWZStatus!) {
        var statusMessage: NSString?
        
        switch status.state {
        case WOWZState.idle:
            playPauseButton.isEnabled = true
            closeButton.isEnabled = true
            statusMessage = StatusMessage.idle as NSString
        case WOWZState.starting:
            playPauseButton.isEnabled = false
            statusMessage = StatusMessage.starting as NSString
            if isStreamingFor == IsSettingFor.VideoStreaming {
                self.wowzaPlayer.playerView = self.playerView;
            }
        case WOWZState.running:
            playPauseButton.isEnabled = true
            infoLabel.isEnabled = false
            closeButton.isEnabled = true
            statusMessage = StatusMessage.running as NSString
        case WOWZState.stopping:
            
            closeButton.isEnabled = true
            statusMessage = StatusMessage.stoping as NSString
        case WOWZState.buffering:
            statusMessage = StatusMessage.buffering as NSString
        case WOWZState.ready:
            statusMessage = StatusMessage.ready as NSString
        }
        if statusMessage != nil {
            print("\(StatusMessage.broadcast) \(String(describing: statusMessage))")
        }
    }
    
    func onWOWZError(_ status: WOWZStatus!) {
        playPauseButton.isEnabled = true
        DispatchQueue.main.async {
            let alert = UIAlertController.init(title: Alert.liveStreamError, message: status.description, preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: Alert.alertTitle, style: .cancel, handler: { (alert) in
                self.playPauseButton.isSelected = false
            }) )
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == WowzaSegueIdentifier.wowzaPlayerSetting {
            let dest = segue.destination as! SettingViewController
            dest.isSettingFor = .wowzaPlayer
        }
    }
    
}

