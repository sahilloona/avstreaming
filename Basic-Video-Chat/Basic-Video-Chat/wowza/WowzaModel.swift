//
//  WowzaModel.swift
//  Basic-Video-Chat
//
//  Created by Chicmic on 06/05/19.
//  Copyright © 2019 tokbox. All rights reserved.
//

import Foundation

struct WowzaConfigure {
     static var hostAddress: String?
     static var portNumber: UInt?
     static var applicationName: String?
     static var streamName: String?
    
     static var username: String?
     static var password: String?
}

struct WowzaAdditionalConfi {
     static var videoFrameRate: UInt?
     static var preRollDuration: Float64?
     static var hlsURL: String?
}




